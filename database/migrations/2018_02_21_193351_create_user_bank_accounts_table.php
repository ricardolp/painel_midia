<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bank_accounts', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('legal_name');
            $table->string('bank_code' );
            $table->integer('agencia')->nullable();
            $table->string('agencia_dv', 1)->nullable();
            $table->integer('conta')->nullable();
            $table->string('conta_dv', 2)->nullable();
            $table->string('type', 30);
            $table->string('document')->nullable();
            $table->string('pagarme_id')->nullable();
            $table->boolean('is_default')->nullable();
            $table->uuid('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bank_accounts');
    }
}
