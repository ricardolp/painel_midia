<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('document', 30);
            $table->string('phone', 30);
            $table->string('address')->nullable();
            $table->string('district')->nullable();
            $table->string('residence_number')->nullable();
            $table->string('residence_complement')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('avatar')->nullable()->default(\URL::to('images/user.png'));
            $table->string('email');
            $table->string('role');
            $table->string('password');
            $table->string('customer_id')->nullable();
            $table->string('path_address')->nullable();
            $table->string('path_default_contract')->nullable();
            $table->string('device_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
