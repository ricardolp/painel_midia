<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPanelRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_panel_requests', function (Blueprint $table) {
            $table->uuid('id');
            $table->integer('status')->default(0);
            $table->string('periodo')->nullable();
            $table->datetime('dt_inicio')->nullable();
            $table->datetime('dt_fim')->nullable();
            $table->string('check_in')->nullable();
            $table->string('check_in_path')->nullable();
            $table->string('check_in_latitude')->nullable();
            $table->string('check_in_longitude')->nullable();
            $table->uuid('user_id');
            $table->uuid('painel_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_panel_requests');
    }
}
