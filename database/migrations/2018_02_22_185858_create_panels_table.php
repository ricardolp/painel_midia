<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('panels', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->string('product')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('address')->nullable();
            $table->string('district')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('number')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('status')->nullable();
            $table->string('thumb')->nullable()->default(\URL::to('images/signal.png'));
            $table->string('car_flow')->nullable();
            $table->string('people_flow')->nullable();
            $table->boolean('is_lighting')->nullable();
            $table->boolean('is_circuit')->nullable();
            $table->boolean('is_restricted')->nullable();
            $table->boolean('is_verified')->default(0);
            $table->boolean('is_weekly')->nullable()->default(0);
            $table->float('value_weekly', 8, 2)->nullable();
            $table->boolean('is_monthly')->nullable()->default(0);
            $table->float('value_monthly', 8, 2)->nullable();
            $table->boolean('is_semester')->nullable()->default(0);
            $table->float('value_semester', 8, 2)->nullable();
            $table->uuid('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panels');
    }
}
