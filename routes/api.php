<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function(){
    Route::middleware('api')->get('get/painel/user/{user}', 'PainelController@paineis');
    Route::middleware('api')->post('login', 'AuthController@login');
    Route::middleware('api')->post('password/reset', 'AuthController@resetPassword');
    Route::middleware('api')->post('password/change', 'AuthController@changePassword');

    Route::middleware('api')->resource('user', 'UserController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.cartao', 'UserCreditCardController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.conta', 'UserBankAccountController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.painel', 'UserPainelController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.demanda', 'UserDemandaController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.favorito', 'UserFavoritosController', ['except' => ['create', 'edit', 'update', 'destroy']]);
    Route::middleware('api')->resource('user.solicitacao', 'UserSolicitacaoController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.solicitacao.checkout', 'UserSolicitacaoCheckoutController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.solicitacao.contrato', 'UserSolicitacaoContratoController', ['except' => ['create', 'edit']]);

    Route::middleware('api')->resource('painel', 'PanelController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('painel.imagem', 'PanelImagemController', ['except' => ['create', 'edit', 'update']]);

    Route::middleware('api')->resource('agencia.solicitacao', 'AgenciaSolicitacaoController', ['except' => ['create', 'edit']]);
    Route::middleware('api')->resource('user.ponto', 'UserPontoController', ['except' => ['create', 'edit', 'update', 'destroy', 'update', 'store', 'show']]);

});