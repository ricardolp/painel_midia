<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.index');
});

Route::group(['namespace' => 'Site'], function(){
    Route::get('/', 'IndexController@index');
    Route::get('/contratacao/{id}', 'IndexController@checkout');
    Route::get('mapa', 'IndexController@mapa');
    Route::get('detalhe/{id}', 'IndexController@painel')->name('site.detalhe');
    Route::get('busca', 'IndexController@busca')->name('site.busca');
    Route::get('mapa/busca', 'IndexController@buscaMapa')->name('site.busca');
    Route::resource('cadastro', 'CadastroController');
    Route::get('data/{data}/{add}', 'IndexController@getDate');
    Route::get('data/{data}/{add}', 'IndexController@getDate');
    Route::post('pagamento', 'PaymentController@store');
    Route::get('pagamento/aguardando', 'PaymentController@aguardando');

});

Route::post('/upload', 'UploadController@upload');

Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('/solicitacoes', 'UserRequestController@index');
    Route::get('/minha-conta', 'UserController@index');
    Route::post('/minha-conta', 'UserController@store');
    Route::get('/minha-conta/senha', 'UserController@password');
    Route::post('/minha-conta/senha', 'UserController@storePassword');
    Route::get('/conta-bancaria', 'UserBankAccountController@index');
    Route::post('/conta-bancaria/destroy', 'UserBankAccountController@destroy');
    Route::post('/conta-bancaria/update', 'UserBankAccountController@update');
    Route::get('/conta-bancaria/create', 'UserBankAccountController@create');
    Route::post('/conta-bancaria', 'UserBankAccountController@store');

    Route::get('create', 'PainelController@create');
    Route::post('create', 'PainelController@store');
    Route::get('edit/{id}', 'PainelController@edit');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
