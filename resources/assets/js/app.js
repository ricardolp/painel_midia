
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./vendor/jquery.bootstrap.wizard.js')
require('./vendor/jquery.validate.min.js')
require('./vendor/paper-bootstrap-wizard.js')
require('./vendor/jquery.mask.min')


$(document).ready(function() {
    $.getJSON('/bancos.json', function (data) {
        var banco = $("#banco");
        var options = '<option value="" disabled selected>Bancos</option>';
        $.each(data, function (key, val) {
            options += '<option value="' + val.code + '" id="' + val.name + '">'+ val.code + ' - ' + val.name + '</option>';
        });
        banco.html(options);
    });

    $(".cnpj").each(function(){
        $(this).keydown(function(){
            try {
                $(".cnpj").unmask();
            } catch (e) {}

            var tamanho = $(".cnpj").val().length;

            if(tamanho < 11){
                $(".cnpj").mask("999.999.999-99");
            } else if(tamanho >= 11){
                $(".cnpj").mask("99.999.999/9999-99");
            }

            var role = document.querySelector('input[name="role"]:checked').value
            if(role === 'agencia'){
                $("#radio").val(1)
            } else {
                $("#radio").val(0)
            }

            // ajustando foco
            var elem = this;
            setTimeout(function(){
                // mudo a posição do seletor
                elem.selectionStart = elem.selectionEnd = 10000;
            }, 0);
            // reaplico o valor para mudar o foco
            var currentValue = $(this).val();
            $(this).val('');
            $(this).val(currentValue);
        });
    })
});

$("#cep").mask("00");
$("#phone").mask("(00) 0 0000-0000");



$("#zipcode").on('change blur keyup keydown', function(){
    var cep = $(this).val()
    if(cep.length >= 9){
        $.get('https://viacep.com.br/ws/'+cep+'/json/', function(data){
            $("#address").val(data.logradouro).change()
            $("#district").val(data.bairro).change()
            $("#state").val(data.uf).change()
            $("#city").val(data.localidade).change()
        })
    }
})

$(".btn-pool").on('click', function(){
    $("#confirmaPular").modal('toggle');
})

$(".btn-next-cfm").on('click', function(){
    $("#pool").val(1)
    $(".btn-next").trigger('click')
    $("#confirmaPular").modal('toggle');
})

$(".btn-confirma").on('click', function(){
    var role = document.querySelector('input[name="role"]:checked').value
    var valid = true;
    if(role === 'agencia'){
        if($("#titular").val() === '') {
            $("#titular").addClass('error')
            valid = false;
        }

        if($("#document").val() === '') {
            $("#document").addClass('error')
            valid = false;
        }

        if($("#agencia").val() === '') {
            $("#agencia").addClass('error')
            valid = false;
            return false;
        }

        if($("#ag_dv").val() === '') {
            $("#ag_dv").addClass('error')
            valid = false;
        }

        if($("#conta").val() === '') {
            $("#conta").addClass('error')
            valid = false;
        }

        if($("#conta_dv").val() === '') {
            $("#conta_dv").addClass('error')
            valid = false;
        }
    } else {
        if($("#name").val() === '') {
            $("#name").addClass('error')
            valid = false;
        }
        if($("#card_name").val() === '') {
            $("#card_name").addClass('error')
            valid = false;
        }
        if($("#card_number").val() === '') {
            $("#card_number").addClass('error')
            valid = false;
        }
        if($("#card_cvv").val() === '') {
            $("#card_cvv").addClass('error')
            valid = false;
        }
    }
    if(!valid) {
        alert('preencha todos os dados')
        return false;
    } else {
        $("#pool").val(1)
        $(".btn-next").trigger('click')
    }
})


$(".upload-contrato").on('click', function () {
    $('#contrato').click();
});

$('#contrato').on('change', function () {
    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[contrato][]" value="' + data.data + '">');
                $(".add-contrato").empty();
                $(".add-contrato").append('<i class="ti-angle-down"></i>');

                if ($("input[name='files[comprovante][]']").length > 0 && $("input[name='files[avatar][]']").length > 0) {
                    $(".btn-unlock").attr('disabled', false);
                }
            }
        });
    }
});

$(".upload-comprovante").on('click', function () {
    $('#comprovante').click();
});

$('#comprovante').on('change', function () {
    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[comprovante][]" value="' + data.data + '">');
                $(".add-comprovante").empty();
                $(".add-comprovante").append('<i class="ft-check"></i>');

                if ($("input[name='files[contrato][]']").length > 0 && $("input[name='files[avatar][]']").length > 0) {
                    $(".btn-unlock").attr('disabled', false);
                }
            }
        });
    }
});

$(".upload-avatar").on('click', function () {
    $('#avatar').click();
});

$('#avatar').on('change', function () {
    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[avatar][]" value="' + data.data + '">');
                $(".add-avatar").empty();
                $(".add-avatar").append('<i class="ft-check"></i>');

                if ($("input[name='files[contrato][]']").length > 0 && $("input[name='files[comprovante][]']").length > 0) {
                    $(".btn-unlock").attr('disabled', false);
                }
            }
        });
    }
});



var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {

    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('in-local')),
        {types: ['geocode']});


    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    var place = document.getElementById('in-local').value;

    const utl = "https://maps.googleapis.com/maps/api/geocode/json?address="+place+"&key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I";

    $.get(utl, function(resp){

        if (resp.status === 'OK') {
            $("#latitude").val(resp.results[0].geometry.location.lat)
            $("#longitude").val(resp.results[0].geometry.location.lng)
        }
    })

}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}
