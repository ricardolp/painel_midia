<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Dashboard eCommerce - Stack Responsive Bootstrap 4 Admin Template</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/app.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/project.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/painel.css">
    <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="content-detached-right-sidebar" class="vertical-layout vertical-menu content-detached-right-sidebar   menu-expanded fixed-navbar">

<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-dark bg-gradient-x-primary navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a href="index-2.html" class="navbar-brand">
                        <img alt="stack admin logo" src="../../../app-assets/images/logo/stack-logo-light.png" class="brand-logo">
                        <h2 class="brand-text">Stack</h2>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div id="navbar-mobile" class="collapse navbar-collapse">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block">
                        <a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs">
                            <i class="ft-menu"></i>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar avatar-online">
                                <img src="{!! Auth::user()->path ?: '/app-assets/images/portrait/small/avatar-s-1.png' !!}" alt="avatar">
                                <i></i>
                            </span>
                            <span class="user-name">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="ft-power"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" navigation-header">
                <span>Menu</span>
                <i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus">
                </i>
            </li>
            <li class=" nav-item">
                <a href="/painel">
                    <i class="ft-file"></i>
                    <span data-i18n="" class="menu-title">Ínicio</span>
                </a>
            </li>
            @if(Auth::user()->role == 'agencia')
            <li class="nav-item">
                <a href="/painel/solicitacoes">
                    <i class="ft-slash"></i>
                    <span data-i18n="" class="menu-title">Solicitações</span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="/painel/conta-bancaria">
                    <i class="ft-life-buoy"></i>
                    <span data-i18n="" class="menu-title">Conta Bancária</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role == 'empresa')
                <li class=" nav-item">
                    <a href="/painel/meus-cartoes">
                        <i class="ft-life-buoy"></i>
                        <span data-i18n="" class="menu-title">Minhas Solicitações</span>
                    </a>
                </li>
                <li class=" nav-item">
                    <a href="/painel/meus-cartoes">
                        <i class="ft-life-buoy"></i>
                        <span data-i18n="" class="menu-title">Meus Cartões</span>
                    </a>
                </li>
                <li class=" nav-item">
                    <a href="/painel/meus-cartoes">
                        <i class="ft-life-buoy"></i>
                        <span data-i18n="" class="menu-title">Meus Pagamentos</span>
                    </a>
                </li>
            @endif
            <li class=" nav-item">
                <a href="/painel/minha-conta">
                    <i class="ft-life-buoy"></i>
                    <span data-i18n="" class="menu-title">Minha Conta</span>
                </a>
            </li>
            @if(Auth::user()->role == 'agencia')
            <li class=" nav-item">
                <a href="/painel/relatórios">
                    <i class="ft-folder"></i>
                    <span data-i18n="" class="menu-title">Relatórios</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            @yield('title')
        </div>
        @yield('content')
    </div>
</div>


<script src="/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="/app-assets/js/core/app.js" type="text/javascript"></script>
<script src="/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
<script src="/js/jquery.mask.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="/js/painel.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3PwGYYFriiwwgfHCt1HHppDhXd6dFZgM&callback=initMap"></script>
</body>
</html>