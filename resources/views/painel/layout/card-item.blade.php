<div class="pipeline-item" onclick="showDataPainel('{{ $painel->id }}')" data-url="{{ URL::to('/') }}">
    <div class="pi-controls">
        @if($painel->is_verified === true)
        <div class="status status-green" data-placement="top" data-toggle="tooltip" title="" data-original-title="Active Status"></div>
        @endif
    </div>

    <div class="bgimg" style="background: url('{{ (!is_null($painel->images)) ? $painel->images->count() > 0 ? $painel->images->first()->path : '' : '' }}')">
    </div>
    <div class="pi-body">
        <div class="pi-info">
            <div class="h6 pi-name">{{ $painel->name }}</div>
            <div class="pi-sub">{{ $painel->address }}</div>
        </div>
    </div>
</div>

