<div class="col-md-8 p-0-30 b-right">
    <div class="card">
        <div class="card-header" id="heading-links">
            <h4 class="card-title">Novo ponto de mídia</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <fieldset class="form-group">
                        <label for="name">Nome</label>
                        <input class="form-control" id="name" name="name" type="text" required>
                    </fieldset>
                </div>
                <div class="col-md-6 col-sm-12">
                    <fieldset class="form-group">
                        <label for="customSelect">Tipo de mídia</label>
                        <select class="custom-select block" name="type" id="customSelect" required>
                            <option selected="">Selecione uma opção</option>
                            <option value="Frontlight">Frontlight</option>
                            <option value="Outdoor">Outdoor</option>
                            <option value="Painel Rodoviario">Painel Rodoviario</option>
                            <option value="Painel Top Sight">Painel Top Sight</option>
                            <option value="Mega Painel">Mega Painel</option>
                            <option value="Empena">Empena</option>
                            <option value="Triedro">Triedro</option>
                            <option value="Painel Eletronico">Painel Eletronico</option>
                            <option value="Bancas">Bancas</option>
                            <option value="Relógios">Relógios</option>
                            <option value="Mobiliário Urbano">Mobiliário Urbano</option>
                            <option value="Busdoor">Busdoor</option>
                        </select>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="state">Estado</label>
                        <select class="form-control estado" id="state" name="state" type="text" required></select>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="city">Cidade</label>
                        <select class="form-control cidade" id="city" name="city" type="text" required></select>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="address">Endereço completo (Rua e número)</label>
                        <input class="form-control" id="address" name="address" placeholder="Ex. Av Sete de Setembro, 1000" type="text" required>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-1">
                    <fieldset class="form-group">
                        <div class="col-md-12 no-padding">
                            <div class="col-md-12 no-padding">
                                <label>Formato (altura x largura)</label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control" id="height" name="height" placeholder="Altura" type="number" required>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" id="width" name="width" placeholder="Largura" type="number" required>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6 mb-1">
                    <fieldset class="form-group">
                        <label for="product">Produção</label>
                        <select class="custom-select block" id="product" name="product" required>
                            <option selected="">Selecione uma opção</option>
                            <option value="Lona">Lona</option>
                            <option value="Papel">Papel</option>
                            <option value="Adesivo">Adesivo</option>
                        </select>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-1">
                    <label for="helperText">Fluxo de carros</label>
                    <fieldset>
                        <div class="input-group">
                            <input class="form-control" name="car_flow" aria-describedby="basic-addon2" type="text" required>
                            <span class="input-group-addon" id="basic-addon2">por dia</span>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6 mb-1">
                    <label for="helperText">Fluxo de pessoas</label>
                    <fieldset>
                        <div class="input-group">
                            <input class="form-control" name="people_flow" aria-describedby="basic-addon2" type="text" required>
                            <span class="input-group-addon" id="basic-addon2">por dia</span>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="col-md-12 m-25">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-4 pl-0">
                            <fieldset class="checkboxsas">
                                <label>
                                    <input name="is_lighting" type="checkbox">
                                    Iluminado
                                </label>
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="checkbox">
                                <label>
                                    <input name="is_circuit" checked="" type="checkbox">
                                    Circuito nobre
                                </label>
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="checkbox">
                                <label>
                                    <input name="is_restricted" checked="" type="checkbox">
                                    Sem Restrição (qualquer tipo de mídia)
                                </label>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 no-padding">
                        <fieldset class="form-group">
                            <label for="placeTextarea">Descrição</label>
                            <textarea class="form-control" name="description" id="placeTextarea" rows="3" placeholder="Descrição da mídia"></textarea>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>