<div class="col-md-12 col-sm-12">
    <div class="card {{ $class }} box-shadow-0">
        <div class="card-body">
            <div class="card-block">
                @if((is_array($data) && count($data) <= 0) || $data->count() === 0)
                    <div class="col-md-12 text-center">
                        Nenhum painel nesta lista
                    </div>
                @endif
                @foreach($data as $painel)
                    @include('painel.layout.card-item', $painel)
                @endforeach
            </div>
        </div>
    </div>
</div>