@section('content')

  <div class="row">
      <div class="col-md-4">
          <div class="card">
              <div id="project-info" class="card-block row">
                  <div class="project-info-count col-lg-12 col-md-12">
                      <div class="project-info-icon">
                          <h2>{{ $info['disponivel'] }}</h2>
                          <div class="project-info-sub-icon">
                              <span class="fa fa fa-calendar-o"></span>
                          </div>
                      </div>
                      <div class="project-info-text pt-1">
                          <h5>Contratos</h5>
                      </div>
                  </div>
              </div>
              <div class="card-block">
                  <div class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2 my-1">
                      <span>Painéis</span>
                  </div>
                  <div class="row py-2">
                      @include('painel.layout.empresa.card-bg',[
                          'class' => 'border-top-green border-top-3',
                          'title' => 'Painéis Contratados',
                          'data' => $paineis['contratado']
                      ])
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-8">
          <div class="card">
              <div id="map-all" style="width: 100%; min-height:600px;"></div>
          </div>
      </div>
  </div>
@endsection

@section('prescript')
    <script>
/*
        var locations = [
            ['Bondi Beach', -33.890542, 151.274856, 4],
            ['Coogee Beach', -33.923036, 151.259052, 5],
            ['Cronulla Beach', -34.028249, 151.157507, 3],
            ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
            ['Maroubra Beach', -33.950198, 151.259302, 1]
        ];
*/

        var user = $("body").data('id')

        var map = new google.maps.Map(document.getElementById('map-all'), {
            zoom: 4,
            center: new google.maps.LatLng(-22.92, -49.25),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        $(document).ready(function(){
            $.get('/api/get/painel/user/'+user, function(data){
                var locations = data.data;
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                        map: map,
                        icon:  '/images/billboard.png'
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i].name);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            });


        })


    </script>
@endsection