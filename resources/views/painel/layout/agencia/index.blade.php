<div class="card p15">
    <div id="project-info" class="card-block row">
        <div class="project-info-count col-lg-4 col-md-12">
            <div class="project-info-icon">
                <h2>{{ $info['disponivel'] }}</h2>
                <div class="project-info-sub-icon">
                    <span class="fa fa fa-calendar-o"></span>
                </div>
            </div>
            <div class="project-info-text pt-1">
                <h5>Disponíveis</h5>
            </div>
        </div>
        <div class="project-info-count col-lg-4 col-md-12">
            <div class="project-info-icon">
                <h2>{{ $info['processo'] }}</h2>
                <div class="project-info-sub-icon">
                    <span class="fa fa-calendar-plus-o"></span>
                </div>
            </div>
            <div class="project-info-text pt-1">
                <h5>Em Processo de Locação</h5>
            </div>
        </div>
        <div class="project-info-count col-lg-4 col-md-12">
            <div class="project-info-icon">
                <h2>{{ $info['contrato'] }}</h2>
                <div class="project-info-sub-icon">
                    <span class="fa fa-calendar-check-o"></span>
                </div>
            </div>
            <div class="project-info-text pt-1">
                <h5>Contratados</h5>
            </div>
        </div>
    </div>
    <!-- project-info -->
    <div class="card-block">
        <div class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
            <span>Painéis</span>
        </div>
        <div class="row py-2">
            @include('painel.layout.card-bg',[
                   'class' => 'border-top-green border-top-3',
                   'title' => 'Painéis Disponíveis',
                   'data' => $paineis['disponivel']
               ])

            @include('painel.layout.card-bg',[
                'class' => 'border-top-blue border-top-3',
                'title' => 'Em Processo de Locação',
                'data' => $paineis['processo']
            ])

            @include('painel.layout.card-bg',[
                'class' => 'border-top-red border-top-3',
                'title' => 'Locados',
                'data' => $paineis['contrato']
            ])
        </div>
    </div>
</div>

<div class="modal fade text-xs-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header header-f6">
                <h4 class="modal-title pull-left" id="myModalLabel1">Ponto de Mídia</h4>

                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close" style="font-size: 2rem;color: red;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="data-painel"></div>
            </div>
        </div>
    </div>
</div>

<a href="/painel/create" class="btn btn-float btn-add-painel"><i class="fa fa-plus"></i></a>