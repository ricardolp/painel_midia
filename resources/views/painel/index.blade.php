@extends('painel.layout')

@section('content')

    @if(Auth::user()->role === 'agencia')
        @include("painel.layout.agencia.index")

    @else

        @include('painel.layout.empresa.index')
    @endif
@endsection