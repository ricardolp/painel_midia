@extends('painel.layout')

@section('content')
    <form action="/painel/create" method="POST" id="painel-create">
        <div class="row">
            @include('painel.layout.form')


            <div class="col-md-4 p-0-40">
                <div class="card">
                    <div class="card-header" id="heading-links">
                        <h4 class="card-title">Novo ponto de mídia</h4>
                    </div>
                    <div class="card-body">
                        <section class="basic-elements">
                            <div class="row">
                                <label>Fotos</label>
                                <div id="recipes"></div>
                                <div class="col-md-12 no-padding mb-15">
                                    <input id="image1" style="display: none" type="file">
                                    <input id="image2" style="display: none" type="file">
                                    <input id="image3" style="display: none" type="file">
                                    <input id="image4" style="display: none" type="file">
                                    <input id="image5" style="display: none" type="file">
                                    <div class="row">
                                        <div class="col-md-6 pr-0">
                                            <div class="image-painel image1"></div>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <div class="imagem-small image2"></div>
                                            <div class="imagem-small image3"></div>
                                            <div class="imagem-small image4"></div>
                                            <div class="imagem-small image5"></div>
                                        </div>
                                    </div>
                                </div>
                                <label>Confirme a localização no mapa</label>
                                <div class="col-md-12 no-padding mb-15">
                                    <div id="map" class="height-300" style="position: relative;"></div>
                                    <input name="latitude" id="latitude" type="hidden">
                                    <input name="longitude" id="longitude" type="hidden">
                                </div>
                                <div class="col-md-12 no-padding mb-15">
                                    <div class="col-md-12 no-padding">
                                        <label>Períodos de locação</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <fieldset class="checkboxsas">
                                                <label>
                                                    <input value="true" name="is_weekly" id="is_weekly" type="checkbox">
                                                    Bi-Semana
                                                </label>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="checkbox">
                                                <label>
                                                    <input value="true" name="is_monthly" id="is_monthly" type="checkbox">
                                                    Mensal
                                                </label>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="checkbox">
                                                <label>
                                                    <input value="true" name="is_semester" id="is_semester" type="checkbox">
                                                    Semestral
                                                </label>
                                            </fieldset>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12 no-padding mb-15">
                                    <fieldset class="form-group">
                                        <div class="col-md-12 no-padding">
                                            <div class="col-md-12 no-padding">
                                                <label>Valor de locação</label>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input class="form-control" id="value_weekly" name="value_weekly" placeholder="Bi-Semana" disabled="" type="text">
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="value_monthly" name="value_monthly" placeholder="Mensal" disabled="" type="text">
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="value_semester" name="value_semester" placeholder="Semestral" disabled="" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success btn-block btn-submit">SALVAR E ENVIAR</button>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
    </form>
    <div class="modal fade" id="confirmapainel" tabindex="-1" role="dialog" aria-labelledby="confirmapainel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Atenção</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Ao clicar em salvar e enviar, seu painel será automáticamente públicado e se todas
                    as informações estiverem corretas já estará disponível para locação, confirme todos
                    os dados para evitar problemas com futuros cliente.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-publicar">Salvar e Publicar</button>
                </div>
            </div>
        </div>
    </div>
@endsection