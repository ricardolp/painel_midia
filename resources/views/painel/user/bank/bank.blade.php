@extends('painel.layout')

@section('content')
    <div class="row">
        <div class="col-xl-8 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Listagem de Contas de Recebimento</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content">
                    @include('error.message')
                    <div class="table-responsive">
                        <table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
                            <thead>
                            <tr>
                                <th>Titular</th>
                                <th>Principal</th>
                                <th>Cod do Banco</th>
                                <th>Agencia</th>
                                <th>Nº da Conta</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($contas as $conta)
                                    <tr>
                                        <td class="text-truncate">{{ $conta->legal_name }}</td>
                                        <td class="text-truncate">{!! $conta->is_default == true ? '<i class="fa fa-check-circle"></i>': '' !!}</td>
                                        <td class="text-truncate">{{ $conta->bank_code }}</td>
                                        <td class="text-truncate">{{ $conta->agencia }}</td>
                                        <td class="text-truncate">{{ $conta->conta }}</td>
                                        <td class="text-truncate">
                                            @if($conta->is_default == false)
                                                <button data-id="{{ $conta->id }}" type="button" class="btn btn-icon btn-default-acc btn-pure success mr-1"><i class="fa fa-check-circle"></i></button>
                                            @endif
                                            <button  data-id="{{ $conta->id }}" type="button" class="btn btn-icon btn-pure btn-remove-bank danger mr-1"><i class="fa fa-close"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="/painel/conta-bancaria/destroy" method="POST" id="form-remove-acc">
        <input type="hidden" name="id" id="bank-val-acc">
    </form>

    <form action="/painel/conta-bancaria/update" method="POST" id="form-update-acc">
        <input type="hidden" name="id" id="bank-default-acc">
    </form>

    <a href="/painel/conta-bancaria/create" class="btn btn-float btn-add-painel"><i class="fa fa-plus"></i></a>
@endsection