@extends('painel.layout')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Minha Conta</h4>
                </div>
                <div class="card-content">
                    @include('error.message')

                    <div class="row">
                        <div class="col-md-12">
                            <form action="/painel/conta-bancaria" method="POST">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset class="form-group">
                                                <label for="basicSelect">Banco</label>
                                                <select class="form-control" id="banco" name="bank_code">
                                                    <option>Selecione</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <fieldset class="form-group">
                                                <label for="type_conta">Tipo de conta</label>
                                                <select class="form-control" id="type_conta" name="type">
                                                    <option>Selecione</option>
                                                    <option value="conta_corrente">Corrente</option>
                                                    <option value="conta_poupanca">Poupança</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <fieldset class="form-group">
                                                <label for="cnpj">Nome do Titular</label>
                                                <input class="form-control titular" id="titular" name="legal_name" placeholder="Nome do Titular da Conta" type="text">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <fieldset class="form-group">
                                                <label for="cnpj">CPF ou CNPJ do Titular</label>
                                                <input class="form-control cnpj" id="document" name="document" placeholder="Digite CPf ou CNPJ" type="text">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <fieldset class="form-group">
                                                        <label for="agencia">Agência</label>
                                                        <input class="form-control" id="agencia" name="agencia" placeholder="Digite a agência" type="text">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="agencia">Digito</label>
                                                        <input class="form-control" id="ag_dv" name="agencia_dv" placeholder="X" type="text">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <fieldset class="form-group">
                                                        <label for="conta">Conta</label>
                                                        <input class="form-control" id="conta" name="conta" placeholder="Digite o Nº da Conta" type="text">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="agencia">Digito</label>
                                                        <input class="form-control" id="conta_dv" name="conta_dv" placeholder="X" type="text">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">Salvar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection