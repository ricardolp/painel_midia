@extends('painel.layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Minha Conta</h4>
                </div>
                <div class="card-content">
                    @include('error.message')

                    <div class="row">
                        <div class="col-md-12">
                            <form action="/painel/minha-conta/senha" method="POST">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Senha Atual</label>
                                                <input class="form-control" id="password" name="password" type="password">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Nova Senha</label>
                                                <input class="form-control" name="new_password" type="password">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Confirmar nova senha</label>
                                                <input class="form-control" name="new_password_confirmation" type="password">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-1">
                                    <button class="btn btn-success">Alterar Senha</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection