@extends('painel.layout')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Minha Conta</h4>
                </div>
                <div class="card-content">
                    @include('error.message')

                    <div class="row">
                        <div class="col-md-12">
                            <form action="/painel/minha-conta" method="POST">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Nome</label>
                                                <input class="form-control" id="name" name="name" type="text" value="{{ Auth::user()->name }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">E-mail</label>
                                                <input class="form-control" id="email" name="email" type="text" disabled value="{{ Auth::user()->email }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Documento</label>
                                                <input class="form-control cnpj" id="cnpj" name="document" type="text" value="{{ Auth::user()->document }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Telfone</label>
                                                <input class="form-control" id="phone" name="phone" type="text" value="{{ Auth::user()->phone }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">CEP</label>
                                                <input class="form-control" id="zipcode" name="zipcode" type="text" value="{{ Auth::user()->zipcode }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-8 col-lg-8 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Endereço</label>
                                                <input class="form-control" id="address" name="address" disabled type="text" value="{{ Auth::user()->address }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Nº</label>
                                                <input class="form-control" id="residence_number" name="residence_number" type="text" value="{{ Auth::user()->residence_number }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-8 col-lg-8 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Bairro</label>
                                                <input class="form-control" id="district" name="district" disabled type="text" value="{{ Auth::user()->district }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Complemento</label>
                                                <input class="form-control" id="residence_complement" name="residence_complement" type="text" value="{{ Auth::user()->residence_complement }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Cidade</label>
                                                <input class="form-control" id="city" name="city" disabled type="text" value="{{ Auth::user()->city }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                                            <fieldset class="form-group">
                                                <label for="basicInput">Estado</label>
                                                <input class="form-control" id="state" name="state" disabled type="text" value="{{ Auth::user()->state }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-1">
                                    <button class="btn btn-success">Salvar</button>
                                    <a href="/painel/minha-conta/senha" class="btn btn-secondary">Alterar Senha</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection