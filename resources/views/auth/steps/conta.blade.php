<div class="col-md-6 col-md-offset-3 role-conta hidden">
    <div class="card no-bg">
        <div class="card-body collapse in card-outline-info no-border-top bg-white">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="basicSelect">Banco</label>
                            <select class="form-control" id="banco" name="conta[bank]">
                                <option>Selecione</option>
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="type_conta">Tipo de conta</label>
                            <select class="form-control" id="type_conta" name="conta[type]">
                                <option>Selecione</option>
                                <option value="conta_corrente">Corrente</option>
                                <option value="conta_poupanca">Poupança</option>
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="cnpj">Nome do Titular</label>
                            <input class="form-control titular" id="titular" name="conta[titular_acc]" placeholder="Nome do Titular da Conta" type="text">
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="cnpj">CPF ou CNPJ do Titular</label>
                            <input class="form-control cnpj" id="document" name="conta[document]" placeholder="Digite CPf ou CNPJ" type="text">
                        </fieldset>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label for="agencia">Agência</label>
                                <input class="form-control" id="agencia" name="conta[agencia]" placeholder="Digite a agência" type="text">
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label for="agencia">Digito</label>
                                <input class="form-control" id="ag_dv" name="conta[agencia-dv]" placeholder="X" type="text">
                            </fieldset>
                        </div>
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label for="conta">Conta</label>
                                <input class="form-control" id="conta" name="conta[conta]" placeholder="Digite o Nº da Conta" type="text">
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label for="agencia">Digito</label>
                                <input class="form-control" id="conta_dv" name="conta[conta-dv]" placeholder="X" type="text">
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <h7>Ao clicar no botão você está aceitamos os nossos <a href="#">Termos & Condições</a>.</h7>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary btn-roxo btn-block btn-next">CONFIRMAR</button>
                    </div>
                    <div class="col-md-12 mt-10">
                        <button type="button" class="btn btn-primary btn-roxo btn-block btn-next">PULAR ETAPA</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>