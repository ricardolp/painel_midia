<div class="col-md-6 col-md-offset-3 role-cartao hidden">
    <div class="card no-bg">
        <div class="card-body collapse in card-outline-info no-border-top bg-white">
            <div class="card-block">
               <div class="row">
                   <div class="col-md-12">
                       <fieldset class="form-group">
                           <label for="card[name]">Nome do Cartão</label>
                           <input class="form-control card[name]" id="card_name" placeholder="Digite o nome do titular" type="text">
                       </fieldset>
                   </div>
                   <div class="col-md-12">
                       <fieldset class="form-group">
                           <label for="card[number]">Número do cartão</label>
                           <input class="form-control card-number" id="card_number" placeholder="Digite o número do cartão" type="text">
                       </fieldset>
                   </div>
                   <div class="col-md-12 no-padding">
                       <div class="col-md-4">
                           <label>Mês</label>
                           <select class="form-control" name="card[mes]">
                               <option value="01">January</option>
                               <option value="02">February </option>
                               <option value="03">March</option>
                               <option value="04">April</option>
                               <option value="05">May</option>
                               <option value="06">June</option>
                               <option value="07">July</option>
                               <option value="08">August</option>
                               <option value="09">September</option>
                               <option value="10">October</option>
                               <option value="11">November</option>
                               <option value="12">December</option>
                           </select>
                       </div>
                       <div class="col-md-4">
                           <label>Ano</label>
                           <select class="form-control" name="card[ano]">
                               <option value="17"> 2017</option>
                               <option value="18"> 2018</option>
                               <option value="19"> 2019</option>
                               <option value="20"> 2020</option>
                               <option value="21"> 2021</option>
                               <option value="22"> 2022</option>
                               <option value="23"> 2023</option>
                           </select>
                       </div>
                       <div class="col-md-4">
                           <fieldset class="form-group">
                               <label for="card[cvv]">CVV</label>
                               <input class="form-control card-cvv" id="card_cvv" name="card[cvv]" type="text"/>
                           </fieldset>
                       </div>

                   </div>
                   <div class="col-md-12 text-center">
                       <h7>Ao clicar no botão você está aceitamos os nossos <a href="#">Termos & Condições</a>.</h7>
                   </div>
                   <div class="col-md-12">
                       <button type="button" class="btn btn-primary btn-roxo btn-block btn-confirma">CONFIRMAR</button>
                   </div>
                   <div class="col-md-12 mt-10">
                       <button type="button" class="btn btn-primary btn-roxo btn-block btn-pool">PULAR ETAPA</button>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>