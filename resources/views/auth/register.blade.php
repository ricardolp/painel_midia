@extends('site.layout.app')

@section('content')
    @include('site.layout.navbar', ['class' => 'inverse'])

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form method="POST" action="/cadastro">
                                {{ csrf_field() }}
                                <input type="hidden" value="0" id="radio" name="radio">
                                <input type="hidden" value="0" id="pool" name="pool">
                                <div class="wizard-navigation">
                                    <div class="progress-with-circle">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="#profile" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-user"></i>
                                                </div>
                                                Perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#details" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-list"></i>
                                                </div>
                                                Cadastro
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#payment" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-money"></i>
                                                </div>
                                                Financeiro
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#documents" data-toggle="tab">
                                                <div class="icon-circle">
                                                    <i class="ti-gallery"></i>
                                                </div>
                                                Documentos
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane" id="profile">
                                        <h5 class="info-text">Selecione o tipo de perfil </h5>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <div class="col-sm-6">
                                                    <div class="choice" data-toggle="wizard-radio">
                                                        <input type="radio" name="role" value="agencia" id="role"/>
                                                        <div class="card card-checkboxes card-hover-effect">
                                                           <img class="card-img img-fluid mb-1" src="/images/site/agencia.png" alt="Card image cap">
                                                            <p>Agência</p>
                                                            <p class="card-text text-center ">Divulgue seus pontos de mídia para milhares de empresas. Check-ins direto no aplicativo!</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="choice" data-toggle="wizard-radio">
                                                        <input type="radio" name="role" value="empresa" id="role"/>
                                                        <div class="card card-checkboxes card-hover-effect">
                                                            <img class="card-img img-fluid mb-1"  src="/images/site/empresario.png" alt="Card image cap">
                                                            <p>Empresa</p>
                                                            <p class="card-text text-center ">Contrate e controle pontos de mídia para divulgar sua empresa em qualquer lugar do mundo!</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="details">
                                        <h5 class="info-text">Preencha seus dados para acessar a plataforma. <br>Os dados serão válidos para o site e aplicativos.</h5>

                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="name">Nome</label>
                                                            <input class="form-control" id="name" name="name" placeholder="Razão social" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="document">CPF ou CNPJ</label>
                                                            <input class="form-control cnpj" id="document" name="document" placeholder="000.000.000-00" type="text">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="phone">Telefone</label>
                                                            <input class="form-control fone" id="phone" name="phone" placeholder="Número de telefone" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="zipcode">CEP</label>
                                                            <input class="form-control" id="zipcode" name="zipcode" data-mask="00000-000" placeholder="Digite seu CEP" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="address">Endereço Completo</label>
                                                            <input class="form-control" id="address" name="address" readonly placeholder="Digite o nome da sua rua" type="text">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="address">Bairro</label>
                                                            <input class="form-control" id="district" name="district" readonly placeholder="Bairro" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="address">Número</label>
                                                            <input class="form-control" id="role" name="residence_number" placeholder="Nº" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <fieldset class="form-group">
                                                            <label for="address">Complemento</label>
                                                            <input class="form-control" id="residence_complement" name="residence_complement" placeholder="Complemento" type="text">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="state">Estado</label>
                                                            <input class="form-control" id="state" readonly name="state" type="text"/>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="city">Cidade</label>
                                                            <input class="form-control" id="city" readonly name="city" type="text">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="email">E-mail</label>
                                                            <input class="form-control" id="email" name="email" placeholder="Email" type="text">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="password">Senha</label>
                                                            <input class="form-control" id="password" name="password" placeholder="Escolha uma senha" type="password">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="payment">

                                        @include('auth.steps.payment')
                                    </div>
                                    <div class="tab-pane" id="documents">
                                        <h5 class="info-text">Anexe seus documentos.</h5>
                                        <div id="recipes"></div>
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div class="col-sm-4">
                                                <input type="file" style="display: none;" id="contrato">
                                                <div class="choice pic upload-contrato">
                                                    <div class="card card-checkboxes card-hover-effect add-contrato">
                                                        <i class="ti-file"></i>
                                                        <p>MODELO DE CONTRATO PADRÃO</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="file" style="display: none;" id="comprovante">

                                                <div class="choice pic upload-comprovante">
                                                    <div class="card card-checkboxes card-hover-effect add-comprovante">
                                                        <i class="ti-map-alt"></i>
                                                        <p>COMPROVANTE DE ENDEREÇO</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="file" style="display: none;" id="avatar">

                                                <div class="choice pic upload-avatar">
                                                    <div class="card card-checkboxes card-hover-effect add-avatar">
                                                        <i class="ti-user"></i>
                                                        <p>FOTO DO PERFIL</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-fill btn-primary btn-wd' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="confirmaPular" tabindex="-1" role="dialog" aria-labelledby="confirmaPular" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja Realemnte pular está etapa?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-next-cfm">Pular</button>
                </div>
            </div>
        </div>
    </div>
    @include('site.layout.footer')
@endsection
