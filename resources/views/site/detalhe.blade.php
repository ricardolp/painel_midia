@extends('site.layout.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/mapa.css') }}">
    <link rel="stylesheet" href="{{ asset('css/detalhe.css') }}">
@endsection

@section('content')

    <header class="he1">
        <div class="logo-topo">
            <a href="{{ URL::to('/') }}"><img src="/images/logo2.png"></a>
        </div>
        <nav class="n1">
            <ul>
                <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
                <li> <a href="#">Aplicativo</a></li>
                <li> <a href="#">Sobre</a></li>
                <li> <a href="{{ URL::to('/cadastro') }}">Cadastrar</a></li>
                <li id="login"><a href="{{ URL::to('/login') }}">Login</a></li>
            </ul>
        </nav>
    </header>

<div class="row">
    <section class="alugarform">
        <div class="col-md-10 col-md-offset-1">
            <form action="/busca" method="get">
                <div id="local">
                    <label for="in-local" id="lbl-in">Local</label>
                    <input id="in-local" name="cidade" type="text" placeholder="Cidade">
                    <input type="hidden" id="latitude" name="latitude">
                    <input type="hidden" id="longitude"  name="longitude">
                    <div id="submit-form2">
                        <input type="submit" value="Buscar espaços" id="submit-button2">
                    </div>
                </div>
            </form>
        </div>
    </section>


    <div class="col-md-10 col-md-offset-1">
        <section class="fotos">
            <div class="col-md-6">
                <div class="content">
                    @foreach($painel->images as $image)
                        <div class="mySlides">
                            <img src="{{ $image->path }}" style="width:100%">
                        </div>
                    @endforeach
                    <div class="caption-container">
                        <p id="caption"></p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($painel->images as $key => $image)
                                <div class="column">
                                    <img class="demo cursor" src="{{ $image->path }}" style="width:100%" onclick="currentSlide({{ $key+1 }})">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="local">
                                @if($painel->is_verified == true)
                                    <p class="verif">Verificado
                                        <img src="/images/ok.png">
                                    </p>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 " >
                            <table class="detalhes">
                                <tr class="text-detalhes">
                                    <th>Detalhes</th>
                                </tr>
                                <tr>
                                    <td><img src="/images/lanterna.png">Iluminado</td>
                                    <td>{{ $painel->is_lighting == true ? 'Sim' : 'Não' }}</td>
                                </tr>
                                <tr>
                                    <td><img src="/images/carro.png">  Fluxo</td>
                                    <td>{{ $painel->car_flow }} veículos por dia</td>
                                </tr>
                                <tr>
                                    <td><img src="/images/point2.png">  Local</td>
                                    <td>Rodovía</td>
                                </tr>
                                <tr>
                                    <td><img src="/images/people.png">  Público</td>
                                    <td>{{ $painel->people_flow }} por dia</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                {{ $painel->description }}
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">

                <section class="coluna-lateral">

                    <div class="col-md-12">
                        <div class="aba">
                            <h4 class="titpanel">{{ $painel->name }}</h4>
                            <p class="text1 ic"><spam>{{ $painel->owner->name }}</spam></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="aba2">
                            <p class="text1">
                            <div class="sign2">
                                <img src="/images/point2.png">
                            </div>
                            {{ $painel->address }}, {{ $painel->city }} - {{ $painel->state }}, {{ $painel->zipcode }}</p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="col-md-12 text-center">
                                    <img class="sign3" src="/images/lona.png">
                                </div>
                                <div class="col-md-12 text-center">
                                    <p class="text2">{{ $painel->product }}</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-12 text-center">
                                    <img class="sign3" src="/images/custo.png">
                                </div>
                                <div class="col-md-12 text-center">
                                    <p class="text2">{{ $value }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="aba4">
                                <div class="title">Perídos de disponibilidade</div>
                                <div class="radio mt-25">
                                    <div class="radio-group">
                                        <input type="radio" id="option-one" name="selector">
                                        <label for="option-one">
                                            <p class="opção">Bi-Semana</p>
                                        </label>
                                        <input type="radio" id="option-two" name="selector">
                                        <label for="option-two">
                                            <p class="opção">Mensal</p>
                                        </label>
                                        <input type="radio" id="option-three" name="selector">
                                        <label for="option-three">
                                            <p class="opção">Semestral</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="aba4">
                                <div class="title">Documentação</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="contrato">
                                <a href="">
                                    <img class="pdf" src="/images/pdf.png">
                                    <p class="download">CONTRATO OFICIAL</p>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="aba4">
                                <div class="title">Pagamento</div>
                            </div>
                            <div class="formas">
                                <img src="{{ asset('images/bandeira.png') }}" width="75%" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="w-75 text-center" style="margin-top: 80px;margin-bottom: 50px">
                                @if(Auth::check())
                                    <a href="/contratacao/{{ $painel->id }}" class="btn-alugar" id="btnAlugar">Alugar</a>
                                @else
                                    <button class="btn-alugar" id="btnAlugar" disabled>Alugar</button>
                                @endif
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </section>
    </div>
</div>
    <footer class="rodape">
        <div class="content">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="org">
                        <img src="/images/logo.png">
                        <p>© 2017 - Direitos reservados.</p>
                        <nav>
                            <ul>
                                <li class="menu"> <a href="index.html">Início</a></li>
                                <li> <a href="index.html">Aplicativo</a></li>
                                <li> <a href="index.html">Sobre</a></li>
                                <li> <a href="index.html">Cadastrar</a></li>
                                <li id="login"> <a href="index.html">Login</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="org2">
                        <form class="formulario2">
                            <div class="col-md-6">
                                <input type="text" id="nome" placeholder="Nome">
                                <input type="text" id="empresa" placeholder="Empresa">
                                <input type="email" id="email-footer" placeholder="E-mail">
                            </div>
                            <div class="col-md-6">
                                <textarea name="message" id="mensagem" placeholder="Mensagem"></textarea>
                                <input type="submit" value="Enviar" id="input-submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </footer>

@endsection

@push('script')
<script type="text/javascript" src="{{ asset('js/slideshow.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/step.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modal.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&libraries=places&callback=initAutocomplete" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/gmap.js') }}"></script>
@endpush



