@extends('site.layout.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/mapa.css') }}">
@endsection

@section('content')


    <header class="he1">
        <div class="logo-topo">
            <img src="/images/logo2.png"></div>
        <nav class="n1">
            <ul>
                <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
                <li> <a href="">Aplicativo</a></li>
                <li> <a href="">Sobre</a></li>
                <li> <a href="{{ URL::to('/cadastro') }}">Cadastrar</a></li>
                <li id="login"> <a href="{{ URL::to('/login') }}">Login</a></li>
            </ul>
        </nav>
    </header>

    <section class="posição-tab">
        <span>
	        <form id="formulario1">
                <ul class="ul2">
                    <li><h1>Pesquisa Rápida</h1></li>
                </ul>
                <div role="main"class="content2">
                    <nav>
                        <div class="col-md-12">
                             <div id="local">
                                <p class="p-go">Local:</p>
                                <input id="in-local" type="text" placeholder="Endereço">
                                 <input type="hidden" id="latitude" name="latitude">
                                 <input type="hidden" id="longitude" name="longitude">
                            </div>
                        </div>
                        <div class="col-md-12">
                             <div id="local">
                                 <p class="p-go">Raio de busca:</p>
                                <input id="in-range" type="range" name="range" min="1" max="500" value="250" onchange="rangeOutputName.value=value">
                                <p class="for-text"><output name="rangeOutputName" id="in-range" >250 </output> Km</p>
                             </div>
                        </div>
                        <div class="col-md-12">
                             <div class="radio">
                                <div class="radio-group">
                                    <input type="radio" id="option-one" name="selector">
                                    <label for="option-one"><p class="opção">Bi-Semana</p></label><input type="radio" id="option-two" name="selector"><label for="option-two"><p class="opção">Mensal</p></label><input type="radio" id="option-three" name="selector"><label for="option-three"><p class="opção">Semestral</p></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <div id="data">
                                <p class="p-go">Data de início:</p>
                                <input id="in-data" type="date" name="data">
                            </div>
                        </div>
                        <div class="col-md-12">
                              <div id="categoria">
                                <p class="p-go">Categoria</p>
                                <select id="in-cat" name="Categoria">
                                <option value="painel">Painel iluminado</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="local">
                                <p class="p-go">Faixa de preço:</p>
                                <input id="in-range" type="range" min="1" max="10000" value="5000" onchange="rangePrice.value=value">
                                <p class="for-text">até R$</p>
                                <output name="rangePrice" id="in-range" >1</output>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="submit-form">
                                <button type="button" value="Buscar espaços" id="submit-button">Buscar espaços</button>
                            </div>
                        </div>
                    </nav>
                </div>
            </form>
        </span>
    </section>
    <div id="floating-panel"></div>
    <section class="posição-mapa">
        <div id="map">
        </div>
    </section>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="{{ asset('/js/mapa.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&libraries=places&callback=initMap" type="text/javascript"></script>
<script src="{{ asset('/js/gmap.js') }}"></script>
<script>
    $(document).ready(function(){
        initAutocomplete()
    })

</script>
@endpush