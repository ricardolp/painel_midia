<footer class="rodape">
    <div class="content">
        <div class="col-md-12">
            <div class="col-md-8">
                <div class="org">
                    <img src="/images/logo.png">
                    <p>© 2017 - Direitos reservados.</p>
                    <nav>
                        <ul>
                            <li class="menu"> <a href="index.html">Início</a></li>
                            <li> <a href="index.html">Aplicativo</a></li>
                            <li> <a href="index.html">Sobre</a></li>
                            <li> <a href="index.html">Cadastrar</a></li>
                            <li id="login"> <a href="index.html">Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-4">
                <div class="org2">
                    <form class="formulario2">
                        <div class="col-md-6">
                            <input type="text" id="nome" placeholder="Nome">
                            <input type="text" id="empresa" placeholder="Empresa">
                            <input type="email" id="email-footer" placeholder="E-mail">
                        </div>
                        <div class="col-md-6">
                            <textarea name="message" id="mensagem" placeholder="Mensagem"></textarea>
                            <input type="submit" value="Enviar" id="input-submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer>