<nav class="navbar navbar-default {!! isset($class) ? $class : '' !!}">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="highlight">
                <img src="{!! isset($class) ? '/images/logo2.png' : '/images/logo1.png' !!}">
            </span>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
            <li> <a href="#">Aplicativo</a></li>
            <li> <a href="#">Sobre</a></li>
            <li> <a href="{{ URL::to('/cadastro') }}">Cadastrar</a></li>
            <li id="login"><a href="{{ URL::to('/login') }}">Login</a></li>
        </ul>
    </div>
</nav>