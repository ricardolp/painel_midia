<div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
          <div class="caixa1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 tab-btn">
                            <h4 class="tit3">Pesquisa rápida</h4>
                        </div>
                        <div class="col-md-6 tab-btn mapa">
                            <a class="botao-mapa" href="{{ URL::to('/mapa') }}">
                                <p class="tit4">Ver mapa</p>
                            </a>
                        </div>
                    </div>
                </div>
              <div class="row">
                  <div class="col-md-12">
                      <form id="formulario1" action="/busca" method="get">
                          <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-8">
                                     <div id="local">
                                         <p class="for-text">Cidade</p>
                                         <input id="in-local" placeholder="Pesquisar cidade" name="cidade" type="text" onFocus="geolocate()" >
                                         <input type="hidden" id="latitude" name="latitude">
                                         <input type="hidden" id="longitude"  name="longitude">
                                     </div>
                                 </div>
                                 <div class="col-md-4 ">
                                     <div id="categoria">
                                         <p class="for-text">Categoria</p>
                                         <select id="in-cat" name="categoria">
                                             <option value="Frontlight">Frontlight</option>
                                             <option value="Outdoor">Outdoor</option>
                                             <option value="Painel Rodoviario">Painel Rodoviario</option>
                                             <option value="Painel To">Painel To</option>
                                             <option value="Mega Painel">Mega Painel</option>
                                             <option value="Empena">Empena</option>
                                             <option value="Triedro">Triedro</option>
                                             <option value="Painel Eletronico">Painel Eletronico</option>
                                             <option value="Bancas">Bancas</option>
                                             <option value="Relógios">Relógios</option>
                                             <option value="Mobiliário Urbano">Mobiliário Urbano</option>
                                             <option value="Busdoo">Busdoo</option>
                                         </select>
                                     </div>
                                 </div>
                             </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div id="sliderkm" class="col-md-4">
                                      <p class="for-text">Raio de busca:</p>
                                      <input id="in-range" name="range" min="1" max="500" value="250" onchange="rangeOutputName.value=value" type="range"/>
                                      <p class="for-text-center">
                                          <output name="rangeOutputName" id="rangeOutputId">250</output> Km
                                      </p>
                                  </div>
                                  <div class="col-md-4">
                                      <div id="data">
                                          <p class="for-text">Início de campanha:</p>
                                          <input id="in-data" name="data" type="date">
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div id="submit-form">
                                          <p></p>
                                          <input value="Buscar espaços" id="submit-button" type="submit">
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>