@extends('site.layout.app')

@section('content')
    <header>
        @include('site.layout.navbar')
        <div class="container">
            <h3 id="tit1">Contrate espaços de mídia em</h3>
            <h3 id="tit2">qualquer lugar do mundo</h3>
        </div>
        @include('site.layout.busca')
    </header>

    <div class="content">
        <section class="cidades">
            <h4 id="tit4">Nas principais cidades do Brasil</h4>
            <br>
            <div class="row">
                <div class="col-md-12">
                    @if(isset($feed['cidade1']) && count($feed['cidade1']) > 1)
                        <p class="background"><span>{{ $feed['cidade1'][0]->city }}</span></p>
                        @foreach($feed['cidade1'] as $item)
                            <div class="boxes">
                                <div class="box col-md-3">
                                    <div class="box-header" style="background: url('{{ ($item->images->count() > 0) ? $item->images->first()->path : URL::to('images/office.jpg') }}');height: 200px;width: 100%;background-position: center center;background-size: 100%;">
                                          <span class="like" >♡</span>

                                    </div>
                                    <h4 class="tit5"><a href="{{ route('site.detalhe', $item->id) }}">{{ "{$item->address} - {$item->district}" }}</a></h4>
                                    <p class="flx"><a href="{{ route('site.detalhe', $item->id) }}">{{ $item->car_flow }} Fluxo de carros | {!! $item->is_lighting == true ? 'Sim' : 'Não' !!} Iluminado</a></p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-12">
                    @if(isset($feed['cidade2']) && count($feed['cidade2']) > 1)
                        <p class="background"><span>{{ $feed['cidade2'][0]->city }}</span></p>
                        @foreach($feed['cidade2'] as $item)
                            <div class="boxes">
                                <div class="box col-md-3">
                                    <div class="box-header" style="background: url('{{ ($item->images->count() > 0) ? $item->images->first()->path : URL::to('images/office.jpg') }}');height: 200px;width: 100%;background-position: center center;background-size: 100%;">
                                        <span class="like" >♡</span>
                                    </div>
                                    <h4 class="tit5"><a href="{{ route('site.detalhe', $item->id) }}">{{ "{$item->address} - {$item->district}" }}</a></h4>
                                    <p class="flx"><a href="{{ route('site.detalhe', $item->id) }}">{{ $item->car_flow }} Fluxo de carros | {!! $item->is_lighting == true ? 'Sim' : 'Não' !!} Iluminado</a></p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
    </div>

    <div class="content">
        <section class="aplicativo">
            <div class="col-md-10 col-md-offset-1">
                <div class="tela col-md-5">
                    <img src="../images/site/telas.png" width="100%">
                </div>
                <div class="par col-md-7">
                    <h1>Aplicativo MediaGO</h1>
                    <p>Pensando no mercado publicitário mundial, que atualmente funciona sem um padrão de qualidade,
                        nós desenvolvemos uma plataforma completa tanto para empresas que buscam espaços de mídia
                        exterior, quando para agências que alugam espaços.</p>

                    <p>Com o MediaGO você pode alugar espaços de mídia em qualquer lugar do mundo.
                        Vocë encontra, alugar e paga  diretamente pelo aplicativo ou website.<br><br>
                        Instale agora em seu smartphone:</p>

                    <div class="getapp ">
                        <a href="">
                            <img src="../images/site/appstore.png" width="150px">
                        </a>
                        <a href="">
                            <img src="../images/site/google-play.png" width="150px">
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content">
        <section class="quem-utiliza">
            <div class="col-md-10 col-md-offset-1">
                <div class="text1 col-md-6">
                    <h1>Quem já utiliza?</h1>
                    <p>Está em dúvida se a plataforma é realmente ideal para sua empresa ou agencia?
                        Nós resolvemos um problema doloroso para grandes empresas, que tinham dificuldade em
                        fazer a gestão de suas campanhas por falta de uma plataforma que oferecesse total controle
                        de mídias, contratos, check-ins e pagamentos em um mesmo lugar.</p>

                    <p>Seus contratos pelo MediaGO tornam-se mais confiáveis, e seus espaços de mídia aparecem
                        para grandes empresas que antes não tinham nenhuma forma de encontra-los.</p>
                    <p>Ainda têm dúvidas sobre nossa plataforma? </p>
                    <a class="botao-roxo" href="">Entrar em contato</a>
                </div>
                <table class="logos col-md-6">
                    <tr>
                        <td align="middle"><img src="/images/site/madero.jpg" width="80%"></td>
                        <td align="middle"><img src="/images/site/sodie.jpg" width="80%"></td>
                    </tr>
                    <tr>
                        <td align="middle"><img src="/images/site/chevrolet.jpg" width="80%"></td>
                        <td align="middle"><img src="/images/site/omar.jpg" width="80%"></td>
                    </tr>
                </table>
            </div>
        </section>
    </div>

    @include('site.layout.footer')
@endsection

@push('script')
<script src="{{ asset('/js/mapa.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&libraries=places&callback=initMap" type="text/javascript"></script>
<script src="{{ asset('/js/gmap.js') }}"></script>
<script>
    $(document).ready(function(){
        initAutocomplete()
    })

</script>
@endpush