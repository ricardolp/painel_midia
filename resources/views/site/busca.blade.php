@extends('site.layout.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/busca.css') }}">
@endsection

@section('content')

    <header class="he1">
        <div class="logo-topo">
            <a href="{{ URL::to('/') }}"><img src="/images/logo2.png"></a>
        </div>
        <nav class="n1">
            <ul>
                <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
                <li> <a href="#">Aplicativo</a></li>
                <li> <a href="#">Sobre</a></li>
                <li> <a href="{{ URL::to('/register') }}">Cadastrar</a></li>
                <li id="login"><a href="{{ URL::to('/login') }}">Login</a></li>
            </ul>
        </nav>
    </header>

    <section class="alugarform">
        <div class="col-md-10 col-md-offset-1">
            <div id="local">
                <p class="for-text">Local</p>
                <input id="in-local" type="text" placeholder="Endereço">
                <div id="submit-form2">
                    <input type="submit" value="Buscar espaços" id="submit-button2">
                </div>
            </div>
        </div>
    </section>

    <section class="busca">
        <div class="col-md-10 col-md-offset-1">
            @if(isset($painels) && count($painels) >= 1)
                <p class="background"><span>Resultado da Busca</span></p>
                @foreach($painels as $item)
                    <div class="boxes">
                        <div class="box col-md-3">
                            <div class="box-header" style="background: url('{{ $item->images->first()->path }}');height: 150px;width: 100%;background-position: center center;background-size: 100%;">

                            </div>
                           {{-- <span class="like" >♡</span>
                            <span class="photo"><img src="/images/photo.png" height="25px" style="max-width: 100%;"></span>--}}
                            <h4 class="tit5"><a href="{{ route('site.detalhe', $item->id) }}">{{ "{$item->address} - {$item->district}" }}</a></h4>
                            <p class="flx"><a href="{{ route('site.detalhe', $item->id) }}">{{ $item->car_flow }} Fluxo de carros | {!! $item->is_lighting == true ? 'Sim' : 'Não' !!} Iluminado</a></p>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-12">
                    <h1 class="not-found">NÃO FOI ENCONTRADO NENHUM PAINEL NA REGIÃO SOLICITADA</h1>
                </div>
            @endif
        </div>

    </section>
@endsection

@push('script')
<script src="{{ asset('/js/mapa.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&callback=initMap" type="text/javascript"></script>
@endpush