<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Media GO</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mapa.css') }}">
    <link rel="stylesheet" href="{{ asset('css/detalhe.css') }}">
    <link rel="stylesheet" href="{{ asset('css/paper-bootstrap-wizard.css') }}">
</head>
<body>

<header class="he1">
    <div class="logo-topo">
        <a href="{{ URL::to('/') }}"><img src="/images/logo2.png"></a>
    </div>
    <nav class="n1">
        <ul>
            <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
            <li> <a href="#">Aplicativo</a></li>
            <li> <a href="#">Sobre</a></li>
            <li> <a href="{{ URL::to('/cadastro') }}">Cadastrar</a></li>
            <li id="login"><a href="{{ URL::to('/login') }}">Login</a></li>
        </ul>
    </nav>
</header>

<div class="col-md-6 col-md-offset-3">
    <div class="wizard-container">
        <div class="card wizard-card" data-color="green" id="wizard">
            <div class="wizard-navigation">
                <div class="progress-with-circle">
                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;">
                    </div>
                </div>
                <ul>
                    <li>
                        <a href="#location" data-toggle="tab">
                            <div class="icon-circle">
                                <i class="ti-map"></i>
                            </div>
                            Confirmado
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane" id="location">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="info-text">Pagamento Efetuado</h5>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="big-confirm">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col-md-12 text-center">
                                    <h4>
                                        Seu pagamento já foi recebido pelo nosso sistema
                                        e está sendo processado, te avisaremos assim que
                                        a operadora liberar o pagamento.
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <a href="/" class="btn btn-success">Voltar ao inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- wizard container -->
</div>
</body>
<script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.bootstrap.wizard.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/paper-bootstrap-wizard.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script>
    $("#data-inicio2").on('change', function(){
        var periodo = document.querySelector('input[name="selector"]:checked').value;
        var data = $(this).val();
        $.ajax({
            method: "GET",
            url: '/data/'+data+'/'+periodo,
        })
        .done(function( data ) {
            $("#data-fim").val(data.data)
        });
    })
</script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&libraries=places&callback=initAutocomplete" async defer></script>--}}
</html>
