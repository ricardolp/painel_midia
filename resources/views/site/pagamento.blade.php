<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Media GO</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mapa.css') }}">
    <link rel="stylesheet" href="{{ asset('css/detalhe.css') }}">
    <link rel="stylesheet" href="{{ asset('css/paper-bootstrap-wizard.css') }}">
</head>
<body>

<header class="he1">
    <div class="logo-topo">
        <a href="{{ URL::to('/') }}"><img src="/images/logo2.png"></a>
    </div>
    <nav class="n1">
        <ul>
            <li class="menu"> <a href="{{ URL::to('/') }}">Início</a></li>
            <li> <a href="#">Aplicativo</a></li>
            <li> <a href="#">Sobre</a></li>
            <li> <a href="{{ URL::to('/cadastro') }}">Cadastrar</a></li>
            <li id="login"><a href="{{ URL::to('/login') }}">Login</a></li>
        </ul>
    </nav>
</header>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="wizard-container">
            <div class="card wizard-card" data-color="green" id="wizard">
                <form action="/pagamento" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="painel_id" value="{{ $painel->id }}">
                    <div class="wizard-navigation">
                        <div class="progress-with-circle">
                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;">
                            </div>
                        </div>
                        <ul>
                            <li>
                                <a href="#location" data-toggle="tab">
                                    <div class="icon-circle">
                                        <i class="ti-map"></i>
                                    </div>
                                    Período
                                </a>
                            </li>
                            <li>
                                <a href="#type" data-toggle="tab">
                                    <div class="icon-circle">
                                        <i class="ti-direction-alt"></i>
                                    </div>
                                    Pagamento
                                </a>
                            </li>
                            <li>
                                <a href="#facilities" data-toggle="tab">
                                    <div class="icon-circle">
                                        <i class="ti-panel"></i>
                                    </div>
                                    Finaliza
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane" id="location">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="info-text">Solicitação de Locação</h5>
                                </div>
                                <div class="col-md-6">
                                    <h4>Periodo:</h4>
                                    <div class="radio mt-25">
                                        <div class="radio-group">
                                            <input id="option-one" value="2" name="selector" type="radio">
                                            <label for="option-one">
                                                <p class="opção">Bi-Semana</p>
                                            </label>
                                            <input id="option-two" value="4" name="selector" type="radio">
                                            <label for="option-two">
                                                <p class="opção">Mensal</p>
                                            </label>
                                            <input id="option-three" value="28" name="selector" type="radio">
                                            <label for="option-three">
                                                <p class="opção">Semestral</p>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <ul class="timeline">
                                            <li class="timeline-inverted">
                                                <div class="timeline-circ circ-xl style-blue"><i class="md md-event"></i></div>
                                                <div class="timeline-entry" style="margin-top: -15px;">
                                                    <div class="card style-default-light nb-cd">
                                                        <div class="card-body small-padding">
                                                            <span class="text-medium color-blue">Início</span><br/>
                                                            <input id="data-inicio2" name="data" type="date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-inverted">
                                                <div class="timeline-circ circ-xl style-red"><i class="md md-event"></i></div>
                                                <div class="timeline-entry" style="margin-top: -15px;">
                                                    <div class="card style-default-light nb-cd">
                                                        <div class="card-body small-padding">
                                                            <span class="text-medium color-red">Fim</span><br/>
                                                            <input id="data-fim" name="data-fim" readonly type="date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bloco-direita">
                                                <h5 class="detail">Detalhes:</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="titpanelD">Painel 23 - BR376</h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="ocl-md-12">
                                                        <p class="text1D p-40"><img class="signD" src="/images/go.png">Publish-on</p>
                                                        <br>
                                                        <p class="text3D p-40 map-icon">BR-376 - Cara-Cara, Ponta Grossa - PR, 84043-460</p>
                                                        <p class="info-pay p-40">Você receberá mais informações sobre a agência assim que finalizar seu pedido.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="type">
                            <h5 class="info-text">Qual a forma de pagamento? </h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="col-sm-12">
                                        <div class="choice active btn-next" onclick="" data-toggle="wizard-checkbox">
                                            <div class="card card-checkboxes card-hover-effect">
                                                <i class="ti-credit-card"></i>
                                                <p>Cartão</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="choice active" data-toggle="wizard-checkbox">
                                            <div class="card card-checkboxes card-hover-effect">
                                                <i class="ti-layout-column4"></i>
                                                <p>Boleto</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bloco-direita">
                                                <h5 class="detail">Detalhes:</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="titpanelD">Painel 23 - BR376</h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="ocl-md-12">
                                                        <p class="text1D p-40"><img class="signD" src="/images/go.png">Publish-on</p>
                                                        <br>
                                                        <p class="text3D p-40 map-icon">BR-376 - Cara-Cara, Ponta Grossa - PR, 84043-460</p>
                                                        <p class="info-pay p-40">Você receberá mais informações sobre a agência assim que finalizar seu pedido.</p>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-6">
                                                                <h5>Valor</h5>
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <h5 id="valor"></h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-12">
                                                                <h3 id="periodo-str"></h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-12">
                                                                <h5 id="periodo-str-dt"></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="facilities">
                            <h5 class="info-text">Tell us more about facilities. </h5>
                            <div class="row">
                                <div class="col-md-6">
                                    {{--
                                                                    @if(Auth::user()->cartao->count() > 0)
                                                                    @endif
                                    --}}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nome do Titular</label>
                                                <input name="holder_name" class="form-control" placeholder="Nome do Títular" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Número do Cartão</label>
                                                <input name="card_number"  class="form-control" data-mask="0000 0000 0000 0000" placeholder="0000 0000 0000 0000" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Validade do Cartão</label>
                                                <input name="validade" class="form-control" placeholder="Ex 02/2018" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>CVV</label>
                                                <input name="cvv" class="form-control" placeholder="Ex 123" max="9999" type="number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 p-40">
                                            <div class="form-group ">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="save">Salvar Cartão para compras futuras</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <button type="submit" class="btn-alugar btn-pagar-painel">Efetuar Pagamento</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bloco-direita">
                                                <h5 class="detail">Detalhes:</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="titpanelD">Painel 23 - BR376</h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="ocl-md-12">
                                                        <p class="text1D p-40"><img class="signD" src="/images/go.png">Publish-on</p>
                                                        <br>
                                                        <p class="text3D p-40 map-icon">BR-376 - Cara-Cara, Ponta Grossa - PR, 84043-460</p>
                                                        <p class="info-pay p-40">Você receberá mais informações sobre a agência assim que finalizar seu pedido.</p>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-6">
                                                                <h5>Valor</h5>
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <h5 id="valor2"></h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-12">
                                                                <h3 id="periodo-str2"></h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-40">
                                                            <div class="col-md-12">
                                                                <h5 id="periodo-str-dt2"></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizard-footer">
                        <div class="pull-right">
                            <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                            <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                        </div>
                        <input type="hidden" id="painel-id" value="{{ $painel->id }}">
                        <div class="pull-left">
                            <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div> <!-- wizard container -->
    </div>

</div>
<footer class="rodape">
    <div class="content">
        <div class="col-md-12">
            <div class="col-md-8">
                <div class="org">
                    <img src="/images/logo.png">
                    <p>© 2017 - Direitos reservados.</p>
                    <nav>
                        <ul>
                            <li class="menu"> <a href="index.html">Início</a></li>
                            <li> <a href="index.html">Aplicativo</a></li>
                            <li> <a href="index.html">Sobre</a></li>
                            <li> <a href="index.html">Cadastrar</a></li>
                            <li id="login"> <a href="index.html">Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-4">
                <div class="org2">
                    <form class="formulario2">
                        <div class="col-md-6">
                            <input type="text" id="nome" placeholder="Nome">
                            <input type="text" id="empresa" placeholder="Empresa">
                            <input type="email" id="email-footer" placeholder="E-mail">
                        </div>
                        <div class="col-md-6">
                            <textarea name="message" id="mensagem" placeholder="Mensagem"></textarea>
                            <input type="submit" value="Enviar" id="input-submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.bootstrap.wizard.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/paper-bootstrap-wizard.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script>
    $("#data-inicio2").on('change', function(){
        var periodo = document.querySelector('input[name="selector"]:checked').value;
        var data = $(this).val();
        $.ajax({
            method: "GET",
            url: '/data/'+data+'/'+periodo,
        })
        .done(function( data ) {
            $("#data-fim").val(data.data)
        });
    })
</script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I&libraries=places&callback=initAutocomplete" async defer></script>--}}
</html>
