$(document).ready(function(){
    $(".cnpj").each(function(){
        $(this).keydown(function(){
            try {
                $(".cnpj").unmask();
            } catch (e) {}

            var tamanho = $(".cnpj").val().length;

            if(tamanho < 11){
                $(".cnpj").mask("999.999.999-99");
            } else if(tamanho >= 11){
                $(".cnpj").mask("99.999.999/9999-99");
            }

            var role = document.querySelector('input[name="role"]:checked').value
            if(role === 'agencia'){
                $("#radio").val(1)
            } else {
                $("#radio").val(0)
            }

            // ajustando foco
            var elem = this;
            setTimeout(function(){
                // mudo a posição do seletor
                elem.selectionStart = elem.selectionEnd = 10000;
            }, 0);
            // reaplico o valor para mudar o foco
            var currentValue = $(this).val();
            $(this).val('');
            $(this).val(currentValue);
        });
    })
});

$.getJSON('/estados-cidades.json', function (data) {
    var estado = $(".estado");

    var options = '<option value="" disabled selected>Estado</option>';

    $.each(data.estados, function (key, val) {
        options += '<option value="' + val.sigla + '" id="' + val.sigla + '">' + val.sigla + '</option>';
    });

    estado.html(options);

    var estado_selected = $("#estado_selected").val();
    if (estado_selected != "") {
        $('#' + estado_selected).attr('selected', true);
    }

    estado.change(function () {
        var cidade = $(".cidade");
        var options_cidades = '<option value="0" selected disabled>Cidade</option>';
        var str = "";

        $(".estado option:selected").each(function () {
            str += $(this).text();
        });

        $.each(data.estados, function (key, val) {
            if (val.sigla == str) {
                $.each(val.cidades, function (key_city, val_city) {
                    options_cidades += '<option value="' + val_city + '" id="' + val_city + '">' + val_city + '</option>';
                });
            }
        });
        cidade.html(options_cidades);

        var cidade_selected = $("#cidade_selected").val();
        if (cidade_selected != "") {
            $('#' + cidade_selected).attr('selected', true);
        }
    }).change();
});


$(".image1").on('click', function () {
    $('#image1').click();
});

$('#image1').on('change', function () {

    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[]" value="' + data.data + '">');
                $(".image-painel.image1").attr('style', 'background: url(' + data.data + ') no-repeat !important;background-size: 100% auto !important;background-position: center center !important;');
            }
        });
    }
});

$(".image2").on('click', function () {
    $('#image2').click();
});

$('#image2').on('change', function () {

    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[]" value="' + data.data + '">');
                $(".image2").attr('style', 'background: url(' + data.data + ') no-repeat !important;background-size: 100% auto !important;background-position: center center !important;');
            }
        });
    }
});

$(".image3").on('click', function () {
    $('#image3').click();
});

$('#image3').on('change', function () {

    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[]" value="' + data.data + '">');
                $(".image3").attr('style', 'background: url(' + data.data + ') no-repeat !important;background-size: 100% auto !important;background-position: center center !important;');
            }
        });
    }
});

$(".image4").on('click', function () {
    $('#image4').click();
});

$('#image4').on('change', function () {

    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[]" value="' + data.data + '">');
                $(".image4").attr('style', 'background: url(' + data.data + ') no-repeat !important;background-size: 100% auto !important;background-position: center center !important;');
            }
        });
    }
});

$(".image5").on('click', function () {
    $('#image5').click();
});

$('#image5').on('change', function () {
    var files = $(this).get(0).files;

    if (files.length > 0) {
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('file[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                $("#recipes").append('<input type="hidden" name="files[]" value="' + data.data + '">');
                $(".image5").attr('style', 'background: url(' + data.data + ') no-repeat !important;background-size: 100% auto !important;background-position: center center !important;');
            }
        });
    }
});


var marker;
var markers;
var infowindow;
var map;
function initMap() {
    var mapCanvas = document.getElementById("map");
    var myCenter= new google.maps.LatLng(-25.508742,-49.120850);
    var mapOptions = {
        center: myCenter, zoom: 5,
        mapTypeId:google.maps.MapTypeId.HYBRID
    };
    map = new google.maps.Map(mapCanvas, mapOptions);
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(map, event.latLng);
        setLatLng(event.latLng)
    });
}

function setLatLng(location)
{
    $("#latitude").val(location.lat())
    $("#longitude").val(location.lng())
}

function placeMarker(map, location) {
    if (!marker || !marker.setPosition) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: '/images/billboard.png'
        });
    } else {
        marker.setPosition(location);
    }
}

$("#address").on('change', function () {
  getGeo();
});

$("#city").on('change', function () {
    getGeo();
});

function getGeo(){
    var address = $("#address").val() + ', ' + $("#state").val() + ', ' + $("#city").val();

    if($("#state").val() != '' && $("#city").val() != '') {
        $.get('http://maps.google.com/maps/api/geocode/json?address=' + address, function (data) {
            $("#address").parent().find('p').remove()
            resultado = data.results;
            if(resultado.length > 0){
                var position = new google.maps.LatLng(resultado[0].geometry.location.lat, resultado[0].geometry.location.lng);

                $("#latitude").val(resultado[0].geometry.location.lat);
                $("#longitude").val(resultado[0].geometry.location.lng);

                map.setCenter(position);
                map.setZoom(16);

                placeMarker(map, position);
            } else {
                $("#address").parent().append('<p class="text-right"><small class="danger text-muted">Insira um endereço váldio (Rua e Número)</small></p>')
            }

        });
    }
}

$("#is_weekly").on('change', function () {
    var chk = $("#is_weekly").is(':checked');

    if (chk === true) {
        $("#value_weekly").attr('disabled', false);
        $("#value_weekly").attr('required', true);
    } else {
        $("#value_weekly").attr('disabled', true);
        $("#value_weekly").attr('required', false);
    }
});

$("#is_monthly").on('change', function () {
    var chk = $("#is_monthly").is(':checked');

    if (chk === true) {
        $("#value_monthly").attr('disabled', false);
        $("#value_monthly").attr('required', true);
    } else {
        $("#value_monthly").attr('disabled', true);
        $("#value_monthly").attr('required', false);
    }
});

$("#is_semester").on('change', function () {
    var chk = $("#is_semester").is(':checked');

    if (chk === true) {
        $("#value_semester").attr('disabled', false);
        $("#value_semester").attr('required', true);
    } else {
        $("#value_semester").attr('disabled', true);
        $("#value_semester").attr('required', false);
    }
});

$(".btn-submit").on('click', function(){
  $("#confirmapainel").modal('toggle')
})

$(".btn-publicar").on('click', function(){
    $("#confirmapainel").modal('toggle')
    $("#painel-create").submit();
})

function showDataPainel(id){
    $("#data-painel").empty()
    $.get('/template/modal.mst', function(tpl) {
        $.get("/api/painel/"+id)
            .done(function( response ) {
                $("#data-painel").append(Mustache.to_html(tpl, response.data));
                $("#default").modal('toggle')
            });
    });

}

$("#cep").mask("00");
$("#phone").mask("(00) 0 0000-0000");
$("#zipcode").on('change blur keyup keydown', function(){
    var cep = $(this).val()
    if(cep.length >= 9){
        $.get('https://viacep.com.br/ws/'+cep+'/json/', function(data){
            $("#address").val(data.logradouro).change()
            $("#district").val(data.bairro).change()
            $("#state").val(data.uf).change()
            $("#city").val(data.localidade).change()
        })
    }
})

$.getJSON('/bancos.json', function (data) {
    var banco = $("#banco");
    var options = '<option value="" disabled selected>Bancos</option>';
    $.each(data, function (key, val) {
        options += '<option value="' + val.code + '" id="' + val.name + '">'+ val.code + ' - ' + val.name + '</option>';
    });
    banco.html(options);
});

$(".btn-remove-bank").on('click', function(){
   $("#bank-val-acc").val($(this).data('id'));
   $("#form-remove-acc").submit();
});


$(".btn-default-acc").on('click', function(){
   $("#bank-default-acc").val($(this).data('id'));
   $("#form-update-acc").submit();
});

