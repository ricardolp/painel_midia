/*!

 =========================================================
 * Paper Bootstrap Wizard - v1.0.2
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/paper-bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Paper Bootstrap Wizard Functions

searchVisible = 0;
transparent = true;

        $(document).ready(function(){

            /*  Activate the tooltips      */
            $('[rel="tooltip"]').tooltip();

            // Code for the Validator
            var $validator = $('.wizard-card form').validate({
        		  rules: {
                      holder_name: {
                        required: true
                      },
                      card_number: {
                        required: true
                      },
                      validade: {
                        required: true
                      },
                      cvv: {
                        required: true
                      }
                },
        	});

            // Wizard Initialization
          	$('.wizard-card').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'nextSelector': '.btn-next',
                'previousSelector': '.btn-previous',

                onNext: function(tab, navigation, index) {
                	var $valid = $('.wizard-card form').valid();
                	if(!$valid) {
                		$validator.focusInvalid();
                		return false;
                	}

                	if(index === 1) {
                	    $(".wizard-footer").addClass('hidden')
                	    var id = $("#painel-id").val()
                        var periodo = document.querySelector('input[name="selector"]:checked').value;
                        $.ajax({
                            method: "GET",
                            url: '/api/painel/'+id,
                        })
                        .done(function( data ) {
                            var result = data.data;
                            var str = '';

                            if(periodo == 2){
                                $("#valor").empty();
                                $("#valor").append('R$ ' + result.value_weekly);
                                $("#valor2").empty();
                                $("#valor2").append('R$ ' + result.value_weekly);
                                str = 'Bi-Semana';
                            }

                            if(periodo == 4){
                                $("#valor").empty();
                                $("#valor").append('R$ ' + result.value_monthly);
                                $("#valor2").empty();
                                $("#valor2").append('R$ ' + result.value_monthly);
                                str = 'Mensal';
                            }

                            if(periodo == 28){
                                $("#valor").empty();
                                $("#valor").append('R$ ' + result.value_semester);
                                $("#valor2").empty();
                                $("#valor2").append('R$ ' + result.value_semester);
                                str = 'Semestral';
                            }

                            $("#periodo-str").empty();
                            $("#periodo-str").append(str)

                            $("#periodo-str2").empty();
                            $("#periodo-str2").append(str)
                            var dt = $("#data-inicio2").val().split('-');
                            var dt2 = $("#data-fim").val().split('-');
                            $("#periodo-str-dt").empty();
                            $("#periodo-str-dt").append('DE: ' + dt[2]+'/'+dt[1]+'/'+dt[0] + ' Até ' + dt2[2]+'/'+dt2[1]+'/'+dt2[0])
                            $("#periodo-str-dt2").empty();
                            $("#periodo-str-dt2").append('DE: ' + dt[2]+'/'+dt[1]+'/'+dt[0] + ' Até ' + dt2[2]+'/'+dt2[1]+'/'+dt2[0])
                        });
                    } else {
                        if(!$(".wizard-footer").hasClass('hidden')) {
                            $(".wizard-footer").addClass('hidden')
                        }
                    }
                },

                onInit : function(tab, navigation, index){

                  //check number of tabs and fill the entire row
                  var $total = navigation.find('li').length;
                  $width = 100/$total;

                  navigation.find('li').css('width',$width + '%');

                },

                onTabClick : function(tab, navigation, index){

                   return false;

                },

                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;

                    var $wizard = navigation.closest('.wizard-card');

                    // If it's the last tab then hide the last button and show the finish instead
                    if($current >= $total) {
                        $($wizard).find('.btn-next').hide();
                        $($wizard).find('.btn-finish').show();
                    } else {
                        $($wizard).find('.btn-next').show();
                        $($wizard).find('.btn-finish').hide();
                    }

                    //update progress
                    var move_distance = 100 / $total;
                    move_distance = move_distance * (index) + move_distance / 2;

                    $wizard.find($('.progress-bar')).css({width: move_distance + '%'});
                    //e.relatedTarget // previous tab

                    $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');

                }
	        });


                // Prepare the preview for profile picture
                $("#wizard-picture").change(function(){
                    readURL(this);
                });


                $('.set-full-height').css('height', 'auto');

            });



         //Function to show image before upload

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
