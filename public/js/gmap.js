
var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {

    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('in-local')),
        {types: ['geocode']});


    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    var place = document.getElementById('in-local').value;

    const utl = "https://maps.googleapis.com/maps/api/geocode/json?address="+place+"&key=AIzaSyD56Twyg3t0xBKi5DZFhWQFm7Lbj7iyU4I";

    $.get(utl, function(resp){

        if (resp.status === 'OK') {
            $("#latitude").val(resp.results[0].geometry.location.lat)
            $("#longitude").val(resp.results[0].geometry.location.lng)
        }
    })

}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
    }
