function initMap() {
	var location = {lat: -23.550520, lng: -46.633309};
	var map= new google.maps.Map(document.getElementById("map"), {
		zoom: 15,
		center: location
	});
	var marker = new google.maps.Marker({
		position:location,
		map: map,
	    icon:'/images/marker1.png'
	});

    marker.addListener('click', function(){
        console.log(this)
    })

}

var objpanel;
var objlocation;
function callback(response) {
    objlocation = response;
}

function callbackpanel(response) {
    objpanel = response;
}


var return_panel = function (id) {
    $.ajax({
        'async': false,
        'type': "get",
        'global': false,
        'url': "/api/painel/"+id,
        'success': function (data) {
            callbackpanel(data.data)
        }
    });
};

var return_first = function () {
    var form = $("#formulario1").serialize();
    $.ajax({
        'async': false,
        'type': "get",
        'global': false,
        'url': "/mapa/busca?"+form,
        'success': function (data) {
            var locat = new Array();
            data = data.result
            data.forEach(function(index){
                locat.push({lat: parseFloat(index.latitude), lng: parseFloat(index.longitude), id: index.id})
            });
            callback(locat);
        }
    });
};

var setFloat = function(painel) {
    return_panel(painel)
    $("#floating-panel").empty();
    $.get('/template/float.mst', function(tpl) {
        $("#floating-panel").append(Mustache.to_html(tpl, objpanel));
        var floatingpanel = document.getElementById("floating-panel");
        floatingpanel.style.display = "block";
    })
}




$("#submit-button").on('click', function(){
    return_first();
    var location = objlocation[0];

    var map= new google.maps.Map(document.getElementById("map"), {
        zoom: 10,
        center: {lat: location.lat, lng: location.lng}
    });
    objlocation.forEach(function (data){
        var marker = new google.maps.Marker({
            position: {lat: data.lat, lng: data.lng},
            map: map,
            icon:'/images/marker1.png',
            customInfo: data.id
        });
        marker.addListener('click', function(){
            setFloat(data.id)
        })
    })
})