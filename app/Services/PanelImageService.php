<?php

namespace App\Services;


use App\Entities\Panel;
use App\Entities\PanelImage;
use App\Entities\User;
use App\Http\Resources\PanelImageResource;
use App\Http\Resources\UserResource;
use App\Http\Controllers\ResponseTrait as Response;
use App\Jobs\User\WelcomeMail;
use App\Validator\PanelImageValidator;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Webpatser\Uuid\Uuid;

class PanelImageService
{

    protected $className = 'Painel';
    /**
     * @var PanelValidator
     */
    private $validator;


    public function __construct(PanelImageValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index($panel)
    {
        $model = PanelImage::where('panel_id', $panel)->get();
        return PanelImageResource::collection($model);
    }

    public function show($uuid)
    {
        $model = PanelImage::find($uuid);

        if(!$model){
            return Response::not_found($this->className);
        }

        return Response::resource(new PanelImageResource($model));
    }

    public function store($request, $panel)
    {
        try{
            $data = $request->all();
            $this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );

            $panel = Panel::find($panel);

            if(!$panel){
                return Response::not_found($this->className);
            }

            $data['panel_id'] = $panel;

            $user = PanelImage::create($data);

            return Response::created($this->className, new PanelImageResource($user));
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
    }

    public function update($request, $uuid)
    {
        try{
            $data = $request->all();
            $this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_UPDATE );

            $model = PanelImage::find($uuid);

            if(!$model){
                return Response::not_found($this->className);
            }

            $model->update($data);
            $model->save();
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
        return Response::updated($this->className, new PanelImageResource($model));
    }

    public function upload($file)
    {
        $name = Uuid::uuid4() . '.png';
        $base64img = preg_replace('#^data:image/\w+;base64,#i', '', $file);
        file_put_contents(storage_path('app/public/'.$name), base64_decode($base64img));
        return URL::to('storage/'.$name);
    }

}