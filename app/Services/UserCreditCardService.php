<?php

namespace App\Services;

use App\Entities\User;
use App\Entities\UserCreditCard;
use App\Http\Resources\UserCreditCardResource;
use App\Validator\UserCreditCardValidator;
use App\Http\Controllers\ResponseTrait as Response;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserCreditCardService
{
    protected $className = 'Cartão de Crédito';
    /**
     * @var UserValidator
     */
    private $validator;


    public function __construct(UserCreditCardValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index($request=null)
    {
        $model = UserCreditCard::all();
        return UserCreditCardResource::collection($model);
    }

    public function show($user, $uuid)
    {
        $user = User::find($user);

        if(!$user){
            return Response::not_found($this->className);
        }

        $card = UserCreditCard::find($uuid);

        if(!$card){
            return Response::not_found($this->className);
        }

        return Response::resource(new UserCreditCardResource($user));
    }

    public function store($request, $user)
    {
        try{
            $data = $request->all();
            $this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );

            $data['user_id'] = $user;
            $user = User::find($user);

            if(!$user){
                return Response::not_found($this->className);
            }

            $check = $user->cartao()->where('is_default', '=', '1')->count();
            $data['is_default'] = $check <= 0 ? true : false.
            $user = UserCreditCard::create($data);

            return Response::created($this->className, new UserCreditCardResource($user));
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
    }


    public function delete($user, $acc)
    {
        $user = User::find($user);

        if(!$user) {
            return Response::not_found('Usuário');
        }

        $acc = UserCreditCard::find($acc);
        $acc->delete();
        return Response::deleted($this->className);

    }


}