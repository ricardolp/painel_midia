<?php

namespace App\Services;


use App\Entities\User;
use App\Http\Resources\UserResource;
use App\Http\Controllers\ResponseTrait as Response;
use App\Jobs\User\WelcomeMail;
use App\Validator\UserValidator;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserService
{

    protected $className = 'Usuário';
    /**
     * @var UserValidator
     */
    private $validator;


    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index($request=null)
    {
        $model = User::all();
        dd($model);
        return UserResource::collection($model);
    }

    public function show($uuid)
    {
        $user = User::find($uuid);

        if(!$user){
            return Response::not_found($this->className);
        }

        return Response::resource(new UserResource($user));
    }

    public function store($request)
    {
        try{
            $data = $request->all();
            $this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );

            $data['avatar'] = isset($data['avatar'])
                ? $this->upload($data['avatar']) : null;
            $data['path_default_contract'] = isset($data['path_default_contract'])
                ? $this->upload($data['path_default_contract']) : null;
            $data['path_address'] = isset($data['path_address'])
                ? $this->upload($data['path_address']) : null;

            $user = User::create($data);

            if($user){
                //dispatch(new WelcomeMail($user));
            }


            return Response::created($this->className, new UserResource($user));
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
    }

    public function update($request, $uuid)
    {

        $data = $request->all();
        $user = User::find($uuid);

        if(!$user){
            return Response::not_found($this->className);
        }

        $data['avatar'] = isset($data['avatar'])
            ? $this->upload($data['avatar']) : $user->avatar;
        $data['path_default_contract'] = isset($data['path_default_contract'])
            ? $this->upload($data['path_default_contract']) : $user->path_default_contract;
        $data['path_address'] = isset($data['path_address'])
            ? $this->upload($data['path_address']) : $user->path_address;

        $user->update($data);
        $user->save();


        return Response::updated($this->className, new UserResource($user));

    }

    public function upload($file)
    {
        //$name = Uuid::uuid() . '.png';
        $name = md5(microtime()) . '.png';
        $base64img = preg_replace('#^data:image/\w+;base64,#i', '', $file);
        file_put_contents(storage_path('app/public/'.$name), base64_decode($base64img));
        return URL::to('storage/'.$name);
    }

}