<?php

namespace App\Services;

use App\Entities\Painel;
use App\Entities\Panel;
use Illuminate\Support\Facades\DB;

class SiteService {

    public static function getfeed(){
        $cities = DB::select('SELECT DISTINCT city FROM panels where (select count(*) from panels as p2 WHERE p2.city=panels.city) >= 4');

        if(count($cities) == 1) {
            $painel = Panel::where('city', $cities[0]->city)->inRandomOrder()->take(4)->get();
            return ['cidade1' => $painel, 'cidade2' => null];
        } elseif (count($cities) > 1) {
            $painel1 = Panel::where('city', $cities[0]->city)->inRandomOrder()->take(4)->get();
            $painel2 = Panel::where('city', $cities[1]->city)->inRandomOrder()->take(4)->get();
            return ['cidade1' => $painel1, 'cidade2' => $painel2];
        } else {
            return ['cidade1' => null, 'cidade2' => null];
        }
    }

    public static function find($query)
    {
        if(!isset($query['cidade'])){
            return false;
        }
        $cidade = explode(',',$query['cidade'] );
        $cidade = trim($cidade[0]);

        return $cidade;
    }

}