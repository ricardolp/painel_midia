<?php

namespace App\Jobs\Pagarme;

use App\Entities\UserBankAccount;
use App\Http\Controllers\PagarmeTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateBankAccount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var UserBankAccount
     */
    private $conta;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserBankAccount $conta)
    {
        //
        $this->conta = $conta;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PagarmeTrait::createBankAccount($this->conta);
    }
}
