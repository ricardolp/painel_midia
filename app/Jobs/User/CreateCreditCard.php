<?php

namespace App\Jobs\User;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateCreditCard implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $card;
    /**
     * @var
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($card, $user)
    {
        $this->card = $card;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arr = [
            "api_key" => env('PAGARME_ID_KEY', 'ak_test_ol9IB6BJI88TJNbxNDqqVCMcAxEgP9'),
            "card_expiration_date" => str_replace('/', '', $this->card['validade']),
            "card_number" => $this->card['card_number'],
            "card_cvv" => $this->card['cvv'],
            "card_holder_name" => $this->card['holder_name'],
        ];

    }
}
