<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'document',
        'phone',
        'avatar',
        'email',
        'role',
        'district',
        'zipcode',
        'residence_number',
        'residence_complement',
        'address',
        'state',
        'city',
        'password',
        'device_token',
        'device_type',
        'path_address',
        'path_default_contract',
        'device_version'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function cartao()
    {
        return $this->hasMany(UserCreditCard::class, 'user_id');
    }

    public function conta()
    {
        return $this->hasMany(UserBankAccount::class, 'user_id');
    }

    public function favorito()
    {
        return $this->hasMany(UserPanelFavorite::class, 'user_id');
    }

    public function demanda()
    {
        return $this->hasMany(UserPanelDemand::class, 'user_id');
    }

    public function solicitacao()
    {
        return $this->hasMany(UserPanelRequest::class, 'user_id');
    }
}
