<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBankAccount extends Model
{
    use Uuids, SoftDeletes;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'legal_name',
        'bank_code',
        'agencia',
        'agencia_dv',
        'conta',
        'conta_dv',
        'type',
        'document',
        'pagarme_id',
        'is_default',
        'user_id',
    ];

}
