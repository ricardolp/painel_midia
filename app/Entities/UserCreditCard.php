<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCreditCard extends Model
{
    use Uuids, SoftDeletes;

    public $incrementing = false;

    protected $fillable = [
        'holder_name',
        'first_digits',
        'brand',
        'pagarme_id',
        'is_default',
        'user_id',
    ];

}
