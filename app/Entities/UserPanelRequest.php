<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserPanelRequest extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'status',
        'periodo',
        'check_in',
        'check_in_path',
        'check_in_latitude',
        'check_in_longitude',
        'dt_inicio',
        'dt_fim',
        'user_id',
        'painel_id'
    ];

    public function painel()
    {
        return $this->belongsTo(Panel::class, 'painel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function contrato()
    {
        return $this->hasMany(UserPanelRequestDocument::class, 'solicitacao_id');
    }

    public function setDtInicioAttribute($value){
        $date = Carbon::createFromFormat('d/m/Y', $value);
        $this->attributes['dt_inicio'] = $date->format('Y-m-d');
    }

    public function setDtFimAttribute($value){
        $date = Carbon::createFromFormat('d/m/Y', $value);
        $this->attributes['dt_fim'] = $date->format('Y-m-d');
    }

    public function getStatusDescAttribute()
    {
        switch ($this->status){
            case 0:
                return 'Solicitação Enviada';
            case 1:
                return 'Solicitação Aceita pela Agência';
            case 2:
                return 'Pagamento Efetuado';
            case 3:
                return 'Contrato Vigente';
            case 4:
                return 'Solicitação Expirada';
            case 5:
                return 'Pagamento Cancelado pela Agência';
            case 6:
                return 'Pagamaneto negado';
            case 7:
                return 'Cancelado';
        }
    }
}
