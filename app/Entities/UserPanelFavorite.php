<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPanelFavorite extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'painel_id',
        'user_id'
    ];

    public function painel()
    {
        return $this->belongsTo(Panel::class, 'painel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
