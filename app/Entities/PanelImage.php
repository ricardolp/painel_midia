<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PanelImage extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'painel_id',
        'path'
    ];
}
