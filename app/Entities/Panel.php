<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Panel extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $appends = ['first_pic'];

    protected $fillable = [
        'name',
        'description',
        'type',
        'city',
        'state',
        'address',
        'district',
        'zipcode',
        'number',
        'thumb',
        'width',
        'height',
        'product',
        'latitude',
        'longitude',
        'car_flow',
        'people_flow',
        'is_lighting',
        'is_circuit',
        'is_restricted',
        'is_weekly',
        'value_weekly',
        'is_monthly',
        'value_monthly',
        'is_semester',
        'value_semester',
        'user_id'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images()
    {
        return $this->hasMany(PanelImage::class, 'painel_id');
    }

    public function getFirstPicAttribute()
    {
        return !is_null($this->images) ? (!is_null($this->images->first())) ? $this->images->first()->path :  URL::to('images/default-painel.jpg') : URL::to('images/default-painel.jpg');
    }

}
