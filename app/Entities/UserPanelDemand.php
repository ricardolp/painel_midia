<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPanelDemand extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'latitude',
        'longitude',
        'raio',
        'description',
        'user_id',
    ];
}
