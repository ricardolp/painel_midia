<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPanelRequestDocument extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'solicitacao_id',
        'path'
    ];
}
