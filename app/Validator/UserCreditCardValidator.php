<?php

namespace App\Validator;

use Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserCreditCardValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'holder_name' => 'required',
            'first_digits' => 'required',
            'brand' => 'required',
            'pagarme_id' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [

        ]
    ];

}