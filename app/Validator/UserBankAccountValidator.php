<?php

namespace App\Validator;

use Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserBankAccountValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'bank_account_id' => 'required',
            'legal_name' => 'required',
            'bank_code' => 'required',
            'agencia' => 'required|integer',
            'agencia_dv' => 'required|integer|max:9',
            'conta' => 'required|integer',
            'conta_dv' => 'required|integer|max:9',
            'type' => 'required|in:conta_corrente,conta_poupanca',
            'document' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [

        ]
    ];

}