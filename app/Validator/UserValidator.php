<?php

namespace App\Validator;

use Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|min:5',
            'document' => 'required',
            'avatar' => 'required',
            'phone' => 'required',
            'password' => 'required|confirmed',
            'role' => 'required|in:empresario,agencia,empresa'
        ],
        ValidatorInterface::RULE_UPDATE => [

        ]
    ];

}