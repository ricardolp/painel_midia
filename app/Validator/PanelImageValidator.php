<?php

namespace App\Validator;

use Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PanelImageValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'path' => 'required|regex:/data:image\/([a-zA-Z]*);base64,([^\"]*)/g',
        ],
        ValidatorInterface::RULE_UPDATE => [
        ]
    ];

}