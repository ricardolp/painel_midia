<?php

namespace App\Validator;

use Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PanelValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required',
            'description' => 'required',
            'type' => 'required|in:Frontlight,Outdoor,Painel Rodoviario,Painel Top Sight,Mega Painel,Empena,Triedro,Painel Eletronico,Bancas,Relógios,Mobiliário Urbano,Busdoor',
            'city' => 'required',
            'state' => 'required',
            'address' => 'required',
            'width' => 'required',
            'height' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'product' => 'required|in:Lona,Papel,Adesivo (triedro)',
            'car_flow'=> 'integer',
            'people_flow'=> 'integer',
            'is_lighting' => 'boolean',
            'is_circuit' => 'boolean',
            'is_restricted' => 'boolean',
            'is_weekly' => 'boolean|required_if:is_monthly,false|required:is_semester,false',
            'value_weekly' => 'required_if:is_weekly,1',
            'is_monthly' => 'boolean|required_if:is_weekly,false|required:is_semester,false',
            'value_monthly' => 'required_if:is_monthly,1',
            'is_semester' => 'boolean|required_if:is_weekly,false|required_if:is_monthly,false',
            'value_semester' => 'required_if:is_semester,1',
            'user_id'=> 'required|exists:users,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'type' => 'in:Frontlight,Outdoor,Painel Rodoviario,Painel Top Sight,Mega Painel,Empena,Triedro,Painel Eletronico,Bancas,Relógios,Mobiliário Urbano,Busdoor',
            'product' => 'in:Lona,Papel,Adesivo (triedro)',
            'car_flow'=> 'integer',
            'people_flow'=> 'integer',
            'is_lighting' => 'boolean',
            'is_circuit' => 'boolean',
            'is_restricted' => 'boolean',
            'is_weekly' => 'boolean|required_if:is_monthly,false|required:is_semester,false',
            'value_weekly' => 'required_if:is_weekly,1',
            'is_monthly' => 'boolean|required_if:is_weekly,false|required:is_semester,false',
            'value_monthly' => 'required_if:is_monthly,1',
            'is_semester' => 'boolean|required_if:is_weekly,false|required_if:is_monthly,false',
            'value_semester' => 'required_if:is_semester,1',
        ]
    ];

}