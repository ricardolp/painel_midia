<?php

namespace App\Http\Controllers\Site;

use App\Entities\User;
use App\Entities\UserBankAccount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CadastroController extends Controller
{

    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if(isset($data['files'])){
            $data['avatar'] = isset($data['files']['avatar']) ? $data['files']['avatar'][0] : null;
            $data['path_address'] = isset($data['files']['comprovante']) ? $data['files']['comprovante'][0] : null;
            $data['path_default_contract'] = isset($data['files']['contrato']) ? $data['files']['contrato'][0] : null;
        }

        $user = User::create($data);

        if($data['radio'] == 0 && $data['pool'] === 0){
            //dispatch(new CreateUserCreditCardOnPagarme($data['card'], $user));
        }

        if($data['radio'] == 1 && $data['pool'] === 0){
            $bank = explode('-', $data['conta']['bank']);
            $document = str_replace('/', '', $data['conta']['document']);
            $document = str_replace('.', '', $document);
            $document = str_replace('-', '', $document);

            $userConta = UserBankAccount::create([
                'legal_name' => $data['conta']['titular_acc'],
                'bank_code' => trim($bank[0]),
                'agencia' => $data['conta']['agencia'],
                'agencia_dv' => $data['conta']['agencia-dv'],
                'conta' => $data['conta']['conta'],
                'conta_dv' => $data['conta']['conta-dv'],
                'type' => $data['conta']['type'],
                'document' => $document,
                'user_id' => $user->id
            ]);

            //dispatch(new CreateUserBankAccountOnPagarme($userConta));
        }

        Auth::loginUsingId($user->id);
        return redirect('/painel');
    }

}
