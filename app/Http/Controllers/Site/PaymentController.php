<?php

namespace App\Http\Controllers\Site;

use App\Entities\UserCreditCard;
use App\Entities\UserPanelRequest;
use App\Jobs\User\CreateCreditCard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PagarMe\Sdk\PagarMe;

class PaymentController extends Controller
{

    public function aguardando()
    {
        return view('site.aguardando');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $dtinicio = Carbon::createFromFormat('Y-m-d', $data['data']);
        $dtfim = Carbon::createFromFormat('Y-m-d', $data['data-fim']);

        $dataArr = [
            'status' => 0,
            'periodo' => $this->getPeriodo($data['selector']),
            'dt_inicio' => $dtinicio->format('d/m/Y') ,
            'dt_fim' => $dtfim->format('d/m/Y'),
            'user_id' => Auth::user()->id,
            'painel_id' => $data['painel_id']
        ];

        $solicitacao = UserPanelRequest::create($dataArr);
        if(isset($data['holder_name']) && !empty($data['holder_name']) && !empty($data['card_number'])){
            dispatch(new CreateCreditCard($data, Auth::user()->id));
        }

        //dispatch(new Solicitacao)
        return redirect('/pagamento/aguardando');
    }

    public function getPeriodo($int)
    {
        switch ($int){
            case 4:
                return 'Bi-Semana';
            case 8:
                return 'Mensal';
            default:
                return 'Semestral';
        }
    }
}
