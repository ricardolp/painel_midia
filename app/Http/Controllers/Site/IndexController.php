<?php

namespace App\Http\Controllers\Site;

use App\Entities\Painel;
use App\Entities\Panel;
use App\Services\SiteService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{

    public function index()
    {
        $feed = SiteService::getfeed();
        return view('site.index')->with(compact('feed'));
    }

    public function mapa()
    {
        return view('site.mapa');
    }

    public function getDate($date, $add)
    {

        $dt = Carbon::createFromFormat('Y-m-d', $date);
        $dt->addWeek($add);
        return ['data' => $dt->format('Y-m-d')];
    }

    public function painel($id)
    {
        $painel = Panel::find($id);
        $value =  $this->getRangeValue($painel->value_semester, $painel->value_monthly, $painel->value_weekly);
        return view('site.detalhe')->with(compact('painel', 'value'));
    }


    public function checkout($painel)
    {
        $painel = Panel::find($painel);
        return view('site.pagamento')->with(compact('painel'));
    }

    public function getRangeValue($a, $b, $c)
    {
        $return = 'R$ ';
        $arr = [$a, $b, $c];
        $min = min($arr) ?: '0';
        $max = max($arr) ?: '0';

        return 'R$ ' . $min . ' - R$ '. $max;
    }

    public function busca(Request $request)
    {

        $page = Input::get('page', 1);
        $perPage = 12;

        $lat =  $request->get('latitude');
        $lng =  $request->get('longitude');
        $raio = $request->query('range') ?: 100;
        $and = " AND type='{$request->query('categoria')}'";

        if(!is_null($lat) && !empty($lat)){

            $result = Panel::select('*', DB::raw("( 3959 * acos( cos( radians(latitude) ) * cos( radians($lat) ) 
   * cos( radians($lng) - radians(longitude)) + sin(radians(latitude)) 
   * sin( radians($lat)))) AS distance"))
                ->having("distance","<=", $raio)
                ->whereRaw("panels.id is not null $and")
                ->orderBy('distance')
                ->get();


            $totalItems = count($result->all());
            $totalPages = ceil($totalItems / $perPage);

            if ($page > $totalPages or $page < 1) {
                $page = 1;
            }

            $offset = ($page * $perPage) - $perPage;
            $painels = array_slice($result->all(), $offset, $perPage);
        } else {
            $result = [];
            $page = 1;
            $offset = 0;
            $totalPages = 1;
        }

        return view('site.busca')->with(compact('painels', 'page', 'offset', 'totalPages'));
    }

    public function buscaMapa(Request $request)
    {

        $lat =  $request->get('latitude');
        $lng =  $request->get('longitude');
        $raio = $request->query('range') ?: 100;


        $result = Panel::select('*', DB::raw("( 3959 * acos( cos( radians(latitude) ) * cos( radians($lat) ) 
   * cos( radians($lng) - radians(longitude)) + sin(radians(latitude)) 
   * sin( radians($lat)))) AS distance"))
            ->having("distance","<=", $raio)
            ->orderBy('distance')
            ->get();
        return ['result' => $result];
    }

}
