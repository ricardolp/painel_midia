<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelImageController extends Controller
{
    protected $service;

    public function __construct(PanelImageService $service)
    {
        $this->service = $service;
    }

    public function index($panel)
    {
        return $this->service->index($panel);
    }

    public function show($panel, $uuid)
    {
        return $this->service->show($panel, $uuid);
    }

    public function store(Request $request, $panel)
    {
        return $this->service->store($request, $panel);
    }

    public function update(Request $request, $panel, $resource)
    {
        return $this->service->update($request, $panel, $resource);
    }
}
