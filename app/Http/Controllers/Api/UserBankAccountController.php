<?php

namespace App\Http\Controllers\Api;


use App\Services\UserBankAccountService;
use Illuminate\Http\Request;

class UserBankAccountController
{
    protected $service;

    public function __construct(UserBankAccountService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function show($user, $uuid)
    {
        return $this->service->show($user, $uuid);
    }

    public function store(Request $request, $user)
    {
        return $this->service->store($request, $user);
    }

    public function update(Request $request, $user, $resource)
    {
        return $this->service->update($request, $user, $resource);
    }

    public function destroy($user, $acc)
    {
        return $this->service->delete($user, $acc);
    }
}