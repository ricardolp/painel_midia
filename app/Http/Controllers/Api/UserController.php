<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function show($uuid)
    {
       return $this->service->show($uuid);
    }

    public function store(Request $request)
    {
        return $this->service->store($request);
    }

    public function update(Request $request, $uuid)
    {
        return $this->service->update($request, $uuid);
    }
}
