<?php

namespace App\Http\Controllers\Api;

use App\Entities\Panel;
use App\Http\Resources\PanelResource;
use App\Services\PanelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait as Response;
use Illuminate\Support\Facades\URL;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Webpatser\Uuid\Uuid;

class PanelController extends Controller
{
    protected $className = 'Painel';

    public function index($request=null)
    {
        $model = Panel::all();
        return PanelResource::collection($model);
    }

    public function show($uuid)
    {
        $user = Panel::find($uuid);
        if(!$user){
            return Response::not_found($this->className);
        }

        return Response::resource(new PanelResource($user));
    }

    public function store($request)
    {
        try{
            $data = $request->all();
            //$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );

            $user = Panel::create($data);

            return Response::created($this->className, new PanelResource($user));
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
    }

    public function update($request, $uuid)
    {
        try{
            $data = $request->all();
            $this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_UPDATE );

            $model = Panel::find($uuid);

            if(!$model){
                return Response::not_found($this->className);
            }

            $model->update($data);
            $model->save();
        } catch (ValidatorException $e){
            return Response::invalid_attr($this->className, $e->getMessageBag());
        }
        return Response::updated($this->className, new PanelResource($model));
    }

    public function upload($file)
    {
        $name = Uuid::uuid4() . '.png';
        $base64img = preg_replace('#^data:image/\w+;base64,#i', '', $file);
        file_put_contents(storage_path('app/public/'.$name), base64_decode($base64img));
        return URL::to('storage/'.$name);
    }
}
