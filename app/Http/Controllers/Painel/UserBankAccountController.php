<?php

namespace App\Http\Controllers\Painel;

use App\Entities\UserBankAccount;
use App\Http\Requests\UserBankAccountRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserBankAccountController extends Controller
{

    public function index()
    {
        $contas = UserBankAccount::where('user_id', Auth::user()->id)->get();
        return view('painel.user.bank.bank')->with(compact('contas'));
    }

    public function create()
    {
        return view('painel.user.bank.create')->with(compact('contas'));
    }

    public function store(UserBankAccountRequest $request)
    {
        $data = $request->all();


        $check = Auth::user()->conta()->where('is_default', '=', true)->where('user_id', Auth::user()->id)->count();

        $data['is_default'] = $check == 0 ? true : false;

        $data['user_id'] = Auth::user()->id;
        UserBankAccount::create($data);
        return redirect('/painel/conta-bancaria')->with('success', 'Conta Bancária Cadatrada com Sucesso');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $conta = UserBankAccount::find($data['id']);

        if(!$conta){
            return back()->with('error', 'Ocorreu um erro ao remover a conta bancária');
        }

        $accs = UserBankAccount::where('user_id', Auth::user()->id)->get();

        foreach ($accs as $acc) {
            $acc->update(['is_default' => false]);
            $acc->save();
        }


        $conta->is_default = true;
        $conta->save();

        return back()->with('success', 'A Conta bancária foi removida com sucesso');
    }

    public function destroy(Request $request)
    {
        $data = $request->all();
        $conta = UserBankAccount::find($data['id']);

        if(!$conta){
            return back()->with('error', 'Ocorreu um erro ao remover a conta bancária');
        }

        $conta->delete();

        return back()->with('success', 'A Conta bancária foi removida com sucesso');
    }
}
