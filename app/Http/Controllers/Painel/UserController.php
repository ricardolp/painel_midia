<?php

namespace App\Http\Controllers\Painel;

use App\Entities\User;
use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index()
    {
        return view('painel.user.index');
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();

        $user = User::find(Auth::user()->id);
        $user->update($data);
        $user->save();

        return back()->with('success', 'Cadastro Atualizado com Sucesso');
    }

    public function password()
    {
        return view('painel.user.senha');
    }

    public function storePassword(UserPasswordRequest $request)
    {
        $data = $request->all();
        $user = User::find(Auth::user()->id);
        if (Hash::check($data['password'], $user->password)) {
            $user->password = $data['new_password'];
            $user->save();
            return back()->with('success', 'Cadastro Atualizado com Sucesso');
        }
        return back()->with('error', 'A Senha atual está incorreta');
    }
}
