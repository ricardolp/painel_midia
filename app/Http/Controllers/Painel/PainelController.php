<?php

namespace App\Http\Controllers\Painel;

use App\Entities\Panel;
use App\Entities\PanelImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PainelController extends Controller
{

    public function create()
    {
        return view('painel.create');
    }

    public function edit($id)
    {
        $painel = Panel::find($id);
        return view('painel.edit')->with(compact('painel'));
    }


    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_circuit'] = isset($data['is_circuit']) && $data['is_circuit'] == 'on' ? true : false;
        $data['is_restricted'] = isset($data['is_restricted']) && $data['is_restricted'] == 'on' ? true : false;
        $data['is_lighting'] = isset($data['is_lighting']) && $data['is_lighting'] == 'on' ? true : false;

        $data['is_weekly'] = isset($data['is_weekly']) && $data['is_weekly'] == 'true' ? true : false;
        $data['is_monthly'] = isset($data['is_monthly']) && $data['is_monthly'] == 'true' ? true : false;
        $data['is_semester'] = isset($data['is_semester']) && $data['is_semester'] == 'true' ? true : false;

        $data['user_id'] = Auth::user()->id;

        $painel = Panel::create($data);
        $painel = Panel::all()->last();

        if(isset($data['files'])){
            foreach ($data['files'] as $file) {
                PanelImage::create([
                    'painel_id' => $painel->id,
                    'path' => $file
                ]);
            }
        }

        //dispatch(new GenerateThumb($painel))->delay(500);

        return redirect('/painel')->with('success', 'Painel adicionado com sucesso');
    }

}
