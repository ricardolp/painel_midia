<?php

namespace App\Http\Controllers\Painel;

use App\Entities\Painel;
use App\Entities\Panel;

use App\Entities\UserPanelRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {
        $id= Auth::user()->id;
        $info = [];

        $paineis = [];
        $paineis['disponivel'] = Panel::whereRaw("
        user_id='$id'
        AND ((panels.id in (SELECT painel_id FROM user_panel_requests WHERE panels.id=user_panel_requests.painel_id and (user_panel_requests.status >= 4))) 
            OR
        (SELECT count(*) FROM user_panel_requests WHERE panels.id=user_panel_requests.painel_id) = 0)")->get();

        $paineis['processo'] = Panel::whereRaw("user_id='$id' AND (panels.id in (SELECT painel_id FROM user_panel_requests WHERE panels.id=user_panel_requests.painel_id and (user_panel_requests.status >= 0 and user_panel_requests.status < 3)))")->get();
        $paineis['contrato'] = Panel::whereRaw("user_id='$id' AND (panels.id in (SELECT painel_id FROM user_panel_requests WHERE panels.id=user_panel_requests.painel_id and (user_panel_requests.status =3)))")->get();

        if(Auth::user()->role != 'agencia'){
            $paineis['contratado'] = UserPanelRequest::join('panels', 'panels.id', '=', 'user_panel_requests.painel_id')
                ->where('user_panel_requests.user_id', '=', $id)
                ->select('panels.*', 'user_panel_requests.user_id as solicitante', 'user_panel_requests.painel_id')
                ->get();
        }

        $info['disponivel'] = $paineis['disponivel']->count();
        $info['processo'] = $paineis['processo']->count();
        $info['contrato'] = $paineis['contrato']->count();


        return view('painel.index')->with(compact('info', 'paineis'));
    }

}
