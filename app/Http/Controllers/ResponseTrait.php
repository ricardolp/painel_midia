<?php

namespace App\Http\Controllers;

trait ResponseTrait {

    public static function resource($resource)
    {
        return response([
            'success' => true,
            'data'    => $resource
        ], 200);
    }

    public static function created($className, $resource)
    {
        return response([
            'success' => true,
            'message' => "$className cadastrado com sucesso",
            'data'    => $resource
        ], 200);
    }

    public static function updated($classname, $resource)
    {
        return response([
            'success' => true,
            'message' => "$classname alterado com sucesso",
            'data'    => $resource
        ], 200);
    }

    public static function deleted($classname)
    {
        return response([
            'success' => true,
            'message' => "$classname deletado com sucesso",
        ], 200);
    }

    public static function not_found($className)
    {
        return response([
            'success' => false,
            'message' => "$className não encontrado"
        ], 404);
    }

    public static function invalid_attr($className, $message)
    {
        return response([
            'success'   => false,
            'message'   => "$className não cadastrado, por favor verifique os campos",
            'errors'    => $message
        ], 422);
    }
}