<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Ramsey\Uuid\Uuid;

class UploadController extends Controller
{

    public function upload(Request $request)
    {
        $data = $request->all();
        $name = Uuid::uuid4();

        $ext = $data['file'][0]->getClientOriginalExtension();
        $data['file'][0]->move(storage_path('app/public/painel'), "$name.$ext");
        return ['success' => true, 'data' => URL::to("storage/painel/$name.$ext")];
    }

}
