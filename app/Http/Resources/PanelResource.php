<?php

namespace App\Http\Resources;

use App\Entities\UserPanelRequest;
use Illuminate\Http\Resources\Json\Resource;

class PanelResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $a = UserPanelRequest::where(['painel_id' => $this->id, ['status', '3']])->count();
        if($a > 0){
            $status = 'Locado';
            $flag = 'danger';
        } else {
            $b = UserPanelRequest::where(['painel_id' => $this->id])->whereBetween('status', [0, 3])->count();

            if($b > 0){
                $status = 'Em Processo de locação';
                $flag = 'info';
            } else {
                $c = UserPanelRequest::where(['painel_id' => $this->id])->where('status', '>=', 4)->count();

                if($c > 0){
                    $status = 'Disponível';
                    $flag = 'primary';
                } else {
                    $status = 'Disponível';
                    $flag = 'primary';
                }
            }
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => $this->type,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'city' => $this->city,
            'state' => $this->state,
            'address' => $this->address,
            'width' => $this->width,
            'thumb' => $this->thumb,
            'height' => $this->height,
            'product' => $this->product,
            'car_flow' => $this->car_flow,
            'people_flow' => $this->people_flow,
            'check_in_path' => $this->check_in_path,
            'is_verified' => is_null($this->is_verified) ? false : (bool) $this->is_verified,
            'is_lighting' => (bool) $this->is_lighting,
            'is_circuit' => (bool) $this->is_circuit,
            'is_restricted' => (bool) $this->is_restricted,
            'is_weekly' => (bool) $this->is_weekly,
            'value_weekly' => $this->value_weekly,
            'is_monthly' => (bool) $this->is_monthly,
            'value_monthly' => $this->value_monthly,
            'is_semester' => (bool) $this->is_semester,
            'value_semester' => $this->value_semester,
            'first_pic' => $this->first_pic,
            'status' => $status,
            'flag' => $flag,
            'rage' => $this->getRangeValue($this->value_semester, $this->value_monthly, $this->value_weekly),
            'owner' => new UserResource($this->owner),
            'imagens' => PanelImageResource::collection($this->images),
        ];
    }

    public function getRangeValue($a, $b, $c)
    {
        $return = 'R$ ';
        $arr = [$a, $b, $c];
        $min = min($arr) ?: '0';
        return 'R$ ' . $min . ' - R$ '. max($arr);
    }

}
