<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user =  [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'document' => $this->document,
            'avatar' => $this->avatar,
            'role' => $this->role,
            'phone' => $this->phone,
        ];

/*        if($this->role == 'agencia'){
            $conta = UserBankAccountResource::collection($this->conta);
            $user['conta'] = $conta;
        } else {

            $cartao = UserCreditCardResource::collection($this->cartao);
            $user['cartao'] = $cartao;
        }*/


        return $user;
    }

}
