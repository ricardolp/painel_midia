<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_code' => 'required',
            'type' => 'required',
            'legal_name' => 'required',
            'document' => 'required',
            'agencia' => 'required',
            'agencia_dv' => 'required',
            'conta' => 'required',
            'conta_dv' => 'required',
        ];
    }
}
